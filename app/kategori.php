<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kategori extends Model
{
    //
    public $table = "tbkategori";

    public function getindikator(){
        return $this->hasMany("App\indikator","idkategori","Id");
    }
    public function getmodel() {
    	return $this->belongsTo("App\\tbmodel", "idmodel","Id");
    }
}
