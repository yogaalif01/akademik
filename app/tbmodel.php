<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbmodel extends Model
{
    //
    public $table = "tbmodel";
    public $timestamps = false;
    protected $guarded = [];
}
