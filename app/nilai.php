<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nilai extends Model
{
    //
    public $table = "tbnilai";
    public $timestamps = false;
    protected $guarded = [];

    public function getindikator() {
    	return $this->belongsTo("App\\indikator", "idindikator","Id");
    }
    public function getsilabus()
    {
    	return $this->belongsTo("App\\silabus","idsilabus","Id");
    }
}
