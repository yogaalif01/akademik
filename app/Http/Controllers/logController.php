<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\guru;
use App\sekolah;
use Session;

class logController extends Controller
{
    /**xx
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cek = sekolah::query()->count();
        if($cek == 0){
            return redirect()->to('/pengaturan+awal');
        }
        if(session("stslogin") == 1){
            return redirect()->back();
        }
        return view("login");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        switch ($request->jenisuser) {
            case 'GURU':
                $guru = guru::where("username",$username)->where("password",$password)->first();
                if($guru <> null)
                {
                    session()->put("stslogin",1);
                    session()->put("loginguru",1);
                    session()->put("nip",$guru->nip);
                    return redirect('/guru');
                }
                else {
                    return redirect('/');
                }
                break;
            
            case 'KEPALA SEKOLAH':
                $guru = guru::where("username",$username)->where("password",$password)->where("jabatan","KEPALA SEKOLAH")->first();
                if($guru <> null){
                    $cek = sekolah::where("nip", $guru->nip)->count();
                    if($cek <> null)
                    {
                        session()->put("stslogin",1);
                        session()->put("loginkepsek",1);
                        session()->put("nip",$guru->nip);
                        return redirect('/kepala+sekolah');
                    }
                    else {
                        return redirect('/');
                    }
                }else{
                    return redirect('/');
                }
                break;
            
            default:
                 return redirect('/');
                break;
        }


        $to = $request->jenisuser == "KEPALA SEKOLAH" ? "/kepala+sekolah" : "/";
        $to = $request->jenisuser == "GURU" ? "/guru" : $to;
        return redirect()->to($to);
    }

    public function pengaturan(){
        return view("pengaturan");
    }

    public function simpanPengaturan(Request $req){
        $req->validate([
            "nip"               => "required",
            "nm_guru"           => "required",
            "alamat_guru"       => "required",
            "tmp_lahir_guru"    => "required",
            "tgl_lahir_guru"    => "required",
            "jk_guru"           => "required",
            "jenis_guru"        => "required",
            "username"          => "required",
            "password"          => "required",
            "nama_sekolah"      => "required",
            "alamat_sekolah"    => "required"
        ],[
            "required"          => "Tidak boleh kosong."
        ]);
        
        $sv                 = new guru;
        $sv->nip            = $req->nip;
        $sv->nama           = $req->nm_guru;
        $sv->tempat_lhr     = $req->tmp_lahir_guru;
        $sv->tgl_lahir      = $req->tgl_lahir_guru;
        $sv->alamat         = $req->alamat_guru;
        $sv->jeniskelamin   = $req->jk_guru;
        $sv->jenis_guru     = $req->jenis_guru;
        $sv->username       = $req->username;
        $sv->password       = $req->password;
        $sv->jabatan        = "KEPALA SEKOLAH";
        $sv->save();

        $sv = new sekolah;
        $sv->nip            = $req->nip;
        $sv->nama_sekolah   = $req->nama_sekolah;
        $sv->alamat_sekolah = $req->alamat_sekolah;
        $sv->save();

        session()->put("stslogin",1);
        session()->put("loginkepsek",1);
        session()->put("nip",$req->nip);

        return redirect('/kepala+sekolah');
    }

    public function logout(){
        Session::flush();
        return redirect()->to("/");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
