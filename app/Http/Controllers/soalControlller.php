<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\soal;

class soalControlller extends Controller
{
    //

    public function pra(){
        $soal = soal::where("kategori", 1)->get();
        return view("kepala sekolah.soal.pra",compact("soal"));
    }

    public function praTambah(){
        return view("kepala sekolah.soal.praTambah");
    }

    public function praHapus($id){
        try {
            soal::where("Id", $id)->delete();
            $msg = "berhasil";
            $sts = 200;
        } catch (Exception $e) {
            //throw $th;
            $msg = $e;
            $sts = 0;
        }
        return json_encode(["status"=>$sts, "message"=>$msg]);
    }

    public function praSimpan(Request $req){
        $req->validate([
            "pertanyaan"    => "required"
        ],[
            "required"      => "Tidak boleh kosong."
        ]);
        
        $sv = new soal;
        $sv->soal = $req->pertanyaan;
        $sv->kategori = 1;
        $sv->save();

        return redirect("/kepala+sekolah/soal+pra.html")->with([
            "status"    => 200, 
            "message"   => "berhasil"
        ]);
    }

    public function praEdit($id){
        $soal = soal::where("Id", $id)->first();
        return view("kepala sekolah.soal.praEdit",compact("soal"));
    }

    public function praSimpanEdit(Request $req){
        $req->validate([
            "pertanyaan"    => "required"
        ],[
            "required"      => "Tidak boleh kosong"
        ]);

        soal::where("Id", $req->idsoal)->update([
            "soal"  => $req->pertanyaan
        ]);

        return redirect("/kepala+sekolah/soal+pra.html")->with([
            "status"    => 200, "message"   => "berhasil"
        ]);
    }

    public function pasca(){
        $soal = soal::where("kategori", 2)->get();
        return view("kepala sekolah.soal.pasca",compact("soal"));
    }

    public function pascaTambah(){
        return view("kepala sekolah.soal.pascaTambah");
    }

    public function pascaSimpan(Request $req){
        $req->validate([
            "pertanyaan"    => "required"
        ],[
            "required"      => "Tidak boleh kosong."
        ]);
        
        $sv = new soal;
        $sv->soal = $req->pertanyaan;
        $sv->kategori = 2;
        $sv->save();

        return redirect("/kepala+sekolah/soal+pasca.html")->with([
            "status"    => 200, 
            "message"   => "berhasil"
        ]);
    }

    public function pascaEdit($id){
        $soal = soal::where("Id", $id)->first();
        return view("kepala sekolah.soal.pascaEdit",compact("soal"));
    }

    public function pascaSimpanEdit(Request $req){
        $req->validate([
            "pertanyaan"    => "required"
        ],[
            "required"      => "Tidak boleh kosong"
        ]);

        soal::where("Id", $req->idsoal)->update([
            "soal"  => $req->pertanyaan
        ]);

        return redirect("/kepala+sekolah/soal+pasca.html")->with([
            "status"    => 200, "message"   => "berhasil"
        ]);
    }

    public function pascaHapus($id){
        try {
            soal::where("Id", $id)->delete();
            $msg = "berhasil";
            $sts = 200;
        } catch (Exception $e) {
            //throw $th;
            $msg = $e;
            $sts = 0;
        }
        return json_encode(["status"=>$sts, "message"=>$msg]);
    }
}
