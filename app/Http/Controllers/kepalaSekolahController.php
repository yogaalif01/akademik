<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\guru;
use App\sekolah;

class kepalaSekolahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view("kepala sekolah.dashboard");
    }

    public function profil(){
        // dd(session("nip"));
        $guru = guru::where("nip", session("nip"))->first();
        return view("kepala sekolah.profil", compact("guru"));
    }

    public function simpanProfilPribadi(Request $req){
        $req->validate([
            "nm_guru"           => "required",
            "alamat_guru"       => "required",
            "tmp_lahir_guru"    => "required",
            "tgl_lahir_guru"    => "required",
            "jk_guru"           => "required",
            "jenis_guru"        => "required",
            "username"          => "required",
            "password"          => "required"
        ],[
            "required"          => "Tidak boleh kosong."
        ]);
        

        guru::where("Id", $req->Id)->update([
            "nama"          => $req->nm_guru,
            "alamat"        => $req->alamat_guru,
            "tempat_lhr"    => $req->tmp_lahir_guru,
            "tgl_lahir"     => $req->tgl_lahir_guru,
            "jeniskelamin"  => $req->jk_guru,
            "jenis_guru"    => $req->jenis_guru,
            "username"      => $req->username,
            "password"      => $req->password
        ]);

        return redirect()->back()->with([
            "status"    => 200, "message"   => "berhasil"
        ]);
    }

    public function silabus(){
        $guru = guru::where("jabatan","<>", "KEPALA SEKOLAH")->get();
        return view("kepala sekolah.nilai", compact("guru"));
    }

    public function guru(){
        $guru = guru::all();
        return view("kepala sekolah.guru.index", compact("guru"));
    }

    public function profilSekolah(){
        $sek = sekolah::first();
        return view("kepala sekolah.sekolah", compact("sek"));
    }

    public function simpanProfilSekolah(Request $req){
        $req->validate([
            "nama_sekolah"  => "required",
            "alamat_sekolah"    => "required",
        ],[
            "required"  => "Tidak boleh kosong"
        ]);

        sekolah::query()->update([
            "nama_sekolah"  => $req->nama_sekolah,
            "alamat_sekolah"    => $req->alamat_sekolah
        ]);

        return redirect()->back()->with([
            "status"    => 200, "meesage"   => "berhasil"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
