<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\guru;
use App\silabus;
use App\nilai;
use App\indikator;
use App\kategori;
use Illuminate\Support\Facades\Storage;

class guruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view("guru.dashboard");
    }
    public function viewnilai()
    {
         $loginguru = session()->get("nip");
        $tpnilai = silabus::with("getnilai")->where("nip",$loginguru)->get();


        return view('guru/nilai', ["data" => $tpnilai]);
    }

    public function profil(Request $request){
        $loginguru = session()->get("nip");

        $profil = guru::where("nip",$loginguru)->first();

        return view("guru.profil", ['profil' => $profil]);
    }

    public function silabus(){
       $loginguru = session()->get("nip");

       $data = silabus::where("nip",$loginguru)->get();

        return view("guru.silabus", ["silabus" => $data]);
    }

    public function tambah(){
        return view("kepala sekolah.guru.create");
    }

    public function simpan(Request $req){
        
        $req->validate([
            "nip"               => "required",
            "nm_guru"           => "required",
            "alamat_guru"       => "required",
            "tmp_lahir_guru"    => "required",
            "tgl_lahir_guru"    => "required",
            "jk_guru"           => "required",
            "jenis_guru"        => "required",
            "username"          => "required",
            "password"          => "required"
        ],[
            "required"          => "Tidak boleh kosong."
        ]);

        $sv = new guru;
        $sv->nip            = $req->nip;
        $sv->nama           = $req->nm_guru;
        $sv->tempat_lhr     = $req->tmp_lahir_guru;
        $sv->tgl_lahir      = $req->tgl_lahir_guru;
        $sv->alamat         = $req->alamat_guru;
        $sv->jeniskelamin   = $req->jk_guru;
        $sv->jenis_guru     = $req->jenis_guru;
        $sv->username       = $req->username;
        $sv->password       = $req->password;
        $sv->jabatan        = "GURU";
        $sv->save();

        return redirect("/kepala+sekolah/guru.html")->with([
            "status"    => 200, "message"   => "berhasil"
        ]);

    }

    public function hapus($id){
        try {
            guru::where("Id", $id)->delete();
            $msg = "berhasil";
            $sts = 200;
        } catch (Exception $e) {
            //throw $th;
            $msg = $e;
            $sts = 0;
        }
        return json_encode(["status"=>$sts, "message"=>$msg]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storesilabus(Request $request)
    {

        $request->validate([
            'berkas' => 'required',
            'model_pembelajaran' => 'required'
        ],[
            'required' => 'Data Tidak Boleh Kosong'
        ]);

        $loginguru = session()->get("nip");
        $file = $request->berkas;
        $fileku = $file->getClientOriginalName();

        $path = $request->file('berkas')->storeAs("file",$fileku,"public");

        
        $silabus = new silabus;
        $silabus->nip = $loginguru;
        $silabus->file = $fileku;
        $silabus->model_pembelajaran = $request->model_pembelajaran;

        $silabus->save();

        return redirect()->to('/guru/silabus+rpp.html')->with(["stsguru" => 1]);
    }

    public function ubah($id){
        $guru = guru::where("Id", $id)->first();
        return view("kepala sekolah.guru.edit", compact("guru"));
    }

    public function simpanEdit(Request $req){
        $req->validate([
            "nip"               => "required",
            "nm_guru"           => "required",
            "alamat_guru"       => "required",
            "tmp_lahir_guru"    => "required",
            "tgl_lahir_guru"    => "required",
            "jk_guru"           => "required",
            "jenis_guru"        => "required",
            "username"          => "required",
            "password"          => "required"
        ],[
            "required"          => "Tidak boleh kosong."
        ]);

        guru::where("Id", $req->Id)->update([
            "nip"           => $req->nip,
            "nama"          => $req->nm_guru,
            "alamat"        => $req->alamat_guru,
            "tempat_lhr"    => $req->tmp_lahir_guru,
            "tgl_lahir"     => $req->tgl_lahir_guru,
            "jeniskelamin"  => $req->jk_guru,
            "jenis_guru"    => $req->jenis_guru,
            "username"      => $req->username,
            "password"      => $req->password
        ]);

        return redirect("/kepala+sekolah/guru.html")->with([
            "status"    => 200, "message"   => "berhasil"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateguru(Request $request)
    {
         $loginguru = session()->get("nip");

        guru::where("nip",$loginguru)->update([

            "nip" => $request->nip,
            "nama"     => $request->nama,
            "tempat_lhr" => $request->tempat_lhr,
            "tgl_lahir"  => date("Y-m-d"),
            "alamat"     => $request->alamat,
            "jenis_guru" => $request->jenis_guru,
            "username"   => $request->username,
            "password"   => $request->password,
            "jabatan"    => $request->jabatan

        ]);

        return redirect()->to('/guru/profil+pribadi.html')->with(["stsguru" => 1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroysilabus($id)
    {
        $silabus = silabus::where("Id",$id)->first();
        storage::disk('public')->delete('/file/'.$silabus->file);

        silabus::where("Id",$id)->delete();

        return redirect()->to('/guru/silabus+rpp.html')->with(["stsguru" => 1]);
    }
    public function viewnilairpp()
    { 
        $loginguru = session()->get("nip");

        $tpnilai = silabus::with("getnilai")->where("nip",$loginguru)->get();
        $guru = silabus::with("getguru")->where("nip",$loginguru)->first();
        
        return view('guru/nilairpp', ["tpnilai" => $tpnilai,"guru" => $guru]);
    }

    public function detailnilai($id)
    {
        $rpp = silabus::with("getguru","getnilai")->where("Id", $id)->first();
        $kategori = kategori::with("getindikator")->orderBy("kode", "ASC")->get();

        $jml = nilai::where("idsilabus",$id)->sum("nilai");
        $maxnilai = 184;
        $total = ($jml / $maxnilai) * 100;
        $total = floor($total);

        return view('guru/detailnilairpp', compact('rpp','kategori','total'));
    }
    public function viewobservasi($id)
    {
         $rpp = silabus::with(["getguru","getnilai" => function($query){
            $query->where("nilai_utk", "observasi pembelajaran");
        } ])->where("Id", $id)->first();
        
        $kategori = kategori::with("getindikator")->where("jenis", "observasi pembelajaran")->orderBy("kode", "ASC")->get();

        $jml = nilai::where("idsilabus",0)->sum("nilai");
        $maxnilai = 224;
        $total = ($jml / $maxnilai) * 100;
        $total = floor($total);

        return view("guru/observasi", compact("rpp","kategori","total"));
    }
}
