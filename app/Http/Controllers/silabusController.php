<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\silabus;
use App\nilai;
use App\kategori;
use App\indikator;
use App\guru;
use App\tbmodel;
use App\soal;
use App\jawaban;

class silabusController extends Controller
{

   public $idmodel;

    public function cari($nip){
        $berkas = silabus::with("getguru","getnilai")->where("nip", $nip)->get();
        if(count($berkas) == 0){
            $result = "<h4 class='page-header page-header-top'>Berkas ditampilkan <small>Download berkas dan beri penilaian</small></h4>
            <br>
            <div class='row'>
                <div class='col-lg-12'>
                    <div class='alert alert-danger'> <strong>Error !</strong> Berkas Tidak Ditemukan</div>
                </div>
            </div>";
        }else{
            $nilai = nilai::where("idsilabus", 0)->average("nilai");
            $nilai = number_format($nilai, 2);
        $result = "
        <div class='row'>
            <div class='col-lg-12'>
                <div class='alert alert-info'>
                <table width='80%'>
                    <tr>
                        <td width='20%'>NIP</td>
                        <td>: {$berkas[0]->getguru->nip}</td>
                    </tr>
                    <tr>
                        <td>NAMA</td>
                        <td>: {$berkas[0]->getguru->nama}</td>
                    </tr>
                    <tr>
                        <td>JENIS GURU</td>
                        <td>: {$berkas[0]->getguru->jenis_guru}</td>
                    </tr>
                </table>
                </div>
            </div>
        </div>
            <br><br>
        <h4 class='page-header page-header-top'>Berkas ditampilkan <small>Download berkas dan beri penilaian</small></h4>
        
        <div class='row'>
            <div class='col-lg-12'>
                <div class='alert alert-info'>
                <b>Informasi !</b> <br>
                <ul>
                    <li>Silahkan Tekan Tombol Berikan Nilai / Revisi Nilai Untuk Memberikan atau Memperbaiki Nilai.</li> 
                    <li>Tekan Tombol [CARI] Sekali Lagi Jika Rata-rata Nilai Yang Ditampilkan Belum Berubah.</li>
                </ul>
                </div>
            </div>
           <div class='col-lg-12'>
              <table class='table table-striped table-hover table-bordered' width='100%' id='tbNilai'>
                 <thead>
                    <tr>
                       <th width='8%'>#</th>
                       <th width='20%'>Nama File</th>
                       <th width='15%'>Nilai RPP</th>
                       <th width='15%'>Pra Observasi</th>
                       <th width='15%'>Observasi Pembelajaran</th>
                       <th width='15%'>Pasca Observasi</th>
                    </tr>
                 </thead>
                 <tbody>";
            $no = 1;
            $url2 = url("kepala+sekolah/simpan+nilai");
            foreach ($berkas as $k) {
            
            $nilai = nilai::where("idsilabus", $k->Id)->where("nilai_utk", "rpp")->average("nilai");
            $nilai = number_format($nilai, 2);
            $label = $nilai == 0 ? "Berikan Nilai" : "Revisi Nilai";

            $nilaiObservasi = nilai::where("idsilabus", $k->Id)->where("nilai_utk", "observasi pembelajaran")->average("nilai");
            $nilaiObservasi = number_format($nilaiObservasi, 2);
            $labelObservasi = $nilaiObservasi == 0 ? "Berikan Nilai" : "Revisi Nilai";

            $url = url("kepala+sekolah/download+berkas/".$k->Id);
            $result .= "<tr>
                        <td>{$no}</td>
                        <td> {$k->file} &nbsp;&nbsp;<a target='_blank' href='{$url}'><span class='fa fa-download'></span> </a> </td>
                        <td>
                             <a href='".url("kepala+sekolah/form+penilaian/".$k->Id)."' target='_blank'> <button class='btn btn-primary btn-sm'>{$label}</button></a>
                            <span class='text-danger' id='span-{$no}'>( {$nilai} )</span>
                        </td>
                        <td>
                            <a target='_blank' href='".url("kepala+sekolah/pra+observasi/".$k->Id)."'> <button class='btn btn-primary btn-sm'>Berikan Nilai</button></a>
                            <span class='text-danger'></span>
                        </td>
                        <td>
                            <a target='_blank'> <button class='btn btn-primary btn-sm' onclick='observasi(".$k->Id.")'>{$labelObservasi}</button></a>
                            <span class='text-danger' >( {$nilaiObservasi} )</span>
                        </td>
                        <td>
                            <a target='_blank' href='".url("kepala+sekolah/pasca+observasi/".$k->Id)."'> <button class='btn btn-primary btn-sm'>Berikan Nilai</button></a>
                            <span class='text-danger'></span>
                        </td>
                    </tr>";
            $no++;
            }
           
        $result .= "</tbody>
              </table>
           </div>
        </div>";
        $model = tbmodel::where("Id","<>",-1)->get();
        $result .= '
                <div id="modelPembelajaran" class="modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4>Model Pembelajaran</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                <input type="hidden" name="" id="idsilabusx">
                                <select name="modelTerpilih" id="modelTerpilih" class="form-control">"';
        foreach ($model as $k) {
        $result .='
                                <option value="'.$k->Id.'">'.$k->nama_model.'</option>';
        }
        
        $result .= '
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success" id="btnObservasi">Lanjut Ke Penilaian</button>
                        </div>
                    </div>
                </div>
            </div>';
        $urlx = url("kepala+sekolah/lembar+observasi+pembelajaran/");
        $result .="
        <script>
        $(function(){
            $('#tbNilai').dataTable();

            $('#btnObservasi').click(function(){
                var idsilabus = $('#idsilabusx').val()
                var idmodel   = $('#modelTerpilih').val()
                window.open('".$urlx."/'+idsilabus+'/'+idmodel, '_blank')
                $('#modelPembelajaran').modal('hide')
            });
        });

        function observasi(idsilabus){
            $('#idsilabusx').val(idsilabus)
            $('#modelPembelajaran').modal()
        }

         function berinilai(id, posisi){
            var nilai = $('#input-'+posisi).val()
            var next = parseInt(posisi) + 1
            var last = {$no} - 1
            var idsilabus = id
            $.ajax({
                url:'{$url2}/'+idsilabus+'/'+nilai,
                type:'GET',
                success:function(r){
                    if(r == 200){
                        $('#span-'+posisi).html('('+nilai+')')
                        $('#input-'+next).focus()
                    }
                    if(last == posisi){
                        Swal.fire(
                            'Berhasil !','Semua berkas telah diberi nilai','success'
                        );
                    }
                },
                error:function(e){
                    console.log(e.responseText);
                }
            });
        }
        </script>";
        }
        return $result;
    }

    public function simpanNilai($id, $nilai){
        $cek = nilai::where("idsilabus", $id)->count();
        if($cek > 0){
            nilai::where("idsilabus", $id)->update(["nilai"=>$nilai]);
        }else{
            nilai::create([
                "idsilabus" => $id,
                "nilai"     => $nilai
            ]);
        }

        return 200;
    }

    public function simpanNilai2(Request $req){
        $kategori = kategori::with("getindikator")->where("jenis", $req->jenis)->whereIn("idmodel", [$req->idmodel,-1])->get();
        foreach ($kategori as $kat) {
            foreach ($kat->getindikator as $i) {
                $vali["nilai".$i->Id] = "required";
            }
        }
        // dd($req->jenis);
        $req->validate($vali, [
            "required"  => "Tidak boleh kosong"
        ]);

        foreach ($kategori as $kate) {
            foreach ($kate->getindikator as $i) {
                $cek = nilai::where("idsilabus", $req->idsilabus)
                ->where("idindikator", $i->Id)
                ->where("nilai_utk", $req->jenis)->count();

                if($cek > 0){
                    nilai::where("idsilabus", $req->idsilabus)
                    ->where("idindikator", $i->Id)
                    ->where("nilai_utk", $req->jenis)->update([
                        "nilai"         =>$req["nilai".$i->Id], 
                        "catatan"       =>$req["catatan".$i->Id]
                    ]);
                }else{
                    nilai::create([
                        "idindikator"   => $i->Id,
                        "nilai"         => $req["nilai".$i->Id],
                        "idsilabus"     => $req->idsilabus,
                        "catatan"       => $req["catatan".$i->Id],
                        "nilai_utk"     => $req->jenis,
                        "idmodel"       => $req["model".$i->Id]
                    ]);
                }
            }
        }
        return "<script>window.close();</script>";
        // return redirect("kepala+sekolah/penilaian.html");
    }

    public function downloadBerkas($id){
        $berkas = silabus::where("Id", $id)->first();
        $path = storage_path("app/public/file/".$berkas->file);

        return response()->download($path);
    }

    public function formNilai($id){
        $rpp = silabus::with(["getguru","getnilai"=>function($q){
            $q->where("nilai_utk","rpp")->get();
        }])->where("Id", $id)->first();
        $kategori = kategori::with("getindikator")->where("jenis", "rpp")->orderBy("kode", "ASC")->get();
        
        return view("kepala sekolah.form-nilai", compact("rpp","kategori"));
    }

    public function formObservasi($id, $model){
        $idmodel = $model;
        $this->idmodel = $model;
        // dd($this->idmodel);
        $rpp = silabus::with(["getguru","getnilai" => function($query){
            $query->where("nilai_utk", "observasi pembelajaran")->whereIn("idmodel",[$this->idmodel, -1]);
        } ])->where("Id", $id)->first();

        $kategori = kategori::with("getindikator","getmodel")->where("jenis", "observasi pembelajaran")
        ->whereIn("idmodel", [$model,-1])->orderBy("kode", "ASC")->get();
        $tbmodel = tbmodel::where("Id", $idmodel)->first();
        
        return view("kepala sekolah.lembar-observasi", compact("kategori","rpp","idmodel","tbmodel"));
    }

    public function praObservasi($idsilabus){
        $rpp = silabus::with(["getguru", "getjawaban"=>function($q){
                $q->where("kategori", 1)->get();
            }])->where("Id", $idsilabus)->first();
        
        $soal = soal::where("kategori", 1)->get();
        return view("kepala sekolah.pra-observasi", compact("rpp","soal"));
    }

    public function praObservasiSimpan(Request $req){
        $soal = soal::where("kategori", 1)->get();
        foreach ($soal as $s) {
            $vali["jawaban".$s->Id] = "required";
        }

        $req->validate($vali,[
            "required"  => "Tidak boleh kosong"
        ]);

        foreach ($soal as $s) {
            if($req->opt == "create"){
                jawaban::create([
                    "idsoal"    => $s->Id,
                    "jawaban"   => $req["jawaban".$s->Id],
                    "nip"       => $req->nip,
                    "idsilabus" => $req->idsilabus,
                    "kategori"  => 1
                ]);
            }else{
                jawaban::where("idsoal", $req["idsoal".$s->Id])->where("idsilabus", $req->idsilabus)
                ->where("kategori", 1)->update([
                    "jawaban"   => $req["jawaban".$s->Id]
                ]);
            }
        }

        return "<script>window.close();</script>";
    }

    public function pascaObservasi($idsilabus){
        $rpp = silabus::with(["getguru", "getjawaban"=>function($q){
                $q->where("kategori", 2)->get();
            }])->where("Id", $idsilabus)->first();
        
        $soal = soal::where("kategori", 2)->get();
        return view("kepala sekolah.pasca-observasi", compact("rpp","soal"));
    }

    public function pascaObservasiSimpan(Request $req){
        $soal = soal::where("kategori", 2)->get();

        foreach ($soal as $s) {
            $vali["jawaban".$s->Id] = "required";
        }

        $req->validate($vali,[
            "required"  => "Tidak boleh kosong"
        ]);

        foreach ($soal as $s) {
            if($req->opt == "create"){
                jawaban::create([
                    "idsoal"    => $s->Id,
                    "jawaban"   => $req["jawaban".$s->Id],
                    "nip"       => $req->nip,
                    "idsilabus" => $req->idsilabus,
                    "kategori"  => 2
                ]);
            }else{
                $ss = jawaban::where("idsoal", $req["idsoal".$s->Id])->where("idsilabus", $req->idsilabus)
                ->where("kategori", 2)->update([
                    "jawaban"   => $req["jawaban".$s->Id]
                ]);
            }
        }

        return "<script>window.close();</script>";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $silabus = silabus::where("Id",$id)->first();
        storage::disk('public')->delete('/file/'.$silabus->file);
    }
}
