<?php

namespace App\Http\Middleware;

use Closure;

class gurumiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->get("stslogin") != 1) {
           return redirect('/');
        }
        else {
            return $next($request);
        }
    }
}
