<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class soal extends Model
{
    //
    public $timestamps = false;
    protected $table = "tbsoal";

    public function getjawaban(){
        return $this->hasMany("App\\jawaban", "idsoal", "Id");
    }
}
