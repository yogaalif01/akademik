<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class silabus extends Model
{
    public $timestamps = false;
    protected $table = "tbsilabus";

    public function getguru(){
        return $this->belongsTo("App\\guru", "nip","nip");
    }

    public function getnilai(){
        return $this->hasMany("App\\nilai","idsilabus","Id");
    }

    public function getjawaban(){
        return $this->hasMany("App\\jawaban", "idsilabus","Id");
    }

}
