<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class indikator extends Model
{
    public $table = "tbindikator";
    public $timestamps = false;
    protected $guarded = [];
}
