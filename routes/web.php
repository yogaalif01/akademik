<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", "logController@index");
Route::post("loginPost", "logController@store");
Route::get("logout.html", "logController@logout");

Route::get("/pengaturan+awal", "logController@pengaturan");
Route::post("/simpanPengaturan", "logController@simpanPengaturan");

Route::group(["prefix"=>"guru","middleware" =>"guru"], function(){
    Route::get("/", "guruController@index");
    Route::get("/profil+pribadi.html", "guruController@profil");
    Route::get("/silabus+rpp.html", "guruController@silabus");
    Route::post("/updateProfil","guruController@updateguru");
    Route::post("/postSilabus","guruController@storesilabus");
    Route::post("/deletesilabus/{id}","guruController@destroysilabus");
    Route::get('/nilaiguru','guruController@viewnilai');
    Route::get('/nilairpp','guruController@viewnilairpp');
    Route::get('/detailrpp/{id}','guruController@detailnilai');
    Route::get('/observasi/{id}','guruController@viewobservasi');
});

Route::group(["prefix"=>"kepala+sekolah", "middleware"=>"kepsek"], function(){
    Route::get("/", "kepalaSekolahController@index");

    Route::get("/profil.html", "kepalaSekolahController@profil");
    Route::post("/simpanProfilPribadi", "kepalaSekolahController@simpanProfilPribadi");

    Route::get("/profil+sekolah.html", "kepalaSekolahController@profilSekolah");    
    Route::post("/simpanProfilSekolah", "kepalaSekolahController@simpanProfilSekolah");

    Route::get("/guru.html", "kepalaSekolahController@guru");
    Route::get("/guru/tambah.html", "guruController@tambah");
    Route::post("/guru/simpan", "guruController@simpan");
    Route::get("/guru/delete-{id}", "guruController@hapus");
    Route::get("/guru/edit-{id}", "guruController@ubah");
    Route::post("/guru/simpanEdit", "guruController@simpanEdit");

    Route::get("/penilaian.html", "kepalaSekolahController@silabus");
    Route::get("/cari+berkas/{nip}", "silabusController@cari");

    Route::get("/form+penilaian/{id}", "silabusController@formNilai");
    Route::get("/lembar+observasi+pembelajaran/{id}/{model}", "silabusController@formObservasi");
    Route::get("/pra+observasi/{idsilabus}", "silabusController@praObservasi");
    Route::post("/pra+observasix/simpan", "silabusController@praObservasiSimpan");
    Route::get("/pasca+observasi/{idsilabus}", "silabusController@pascaObservasi");
    Route::post("/pasca+observasix/simpan", "silabusController@pascaObservasiSimpan");
    
    Route::post("/simpan+nilai", "silabusController@simpanNilai2");
    Route::get("/simpan+nilai/{id}/{nilai}", "silabusController@simpanNilai");
    Route::get("/download+berkas/{id}", "silabusController@downloadBerkas");

    Route::get("/soal+pra.html", "soalControlller@pra");
    Route::get("/soal+pra/tambah.html", "soalControlller@praTambah");
    Route::post("/soal+pra/simpan", "soalControlller@praSimpan");
    Route::get("/soal+pra/delete-{id}", "soalControlller@praHapus");
    Route::get("soal+pra/edit-{id}", "soalControlller@praEdit");
    Route::post("soal+pra/simpanEdit", "soalControlller@praSimpanEdit");

    Route::get("/soal+pasca.html", "soalControlller@pasca");
    Route::get("/soal+pasca/tambah.html", "soalControlller@pascaTambah");
    Route::post("/soal+pasca/simpan", "soalControlller@pascaSimpan");
    Route::get("soal+pasca/edit-{id}", "soalControlller@pascaEdit");
    Route::post("soal+pasca/simpanEdit", "soalControlller@pascaSimpanEdit");
    Route::get("/soal+pasca/delete-{id}", "soalControlller@pascaHapus");
});

// testing