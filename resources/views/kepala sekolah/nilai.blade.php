@extends('kepala sekolah.template')
@section('nilai','active')
@section('header')
<!-- Navigation info -->
<ul id="nav-info" class="clearfix">
   <li><a href="{{url('/kepala+sekolah')}}"><i class="fa fa-home"></i></a></li>
   <li class="active"><a href="">Penilaian</a></li>
</ul>
<!-- END Navigation info -->

<!-- Your Content -->
<h3 class="page-header page-header-top"><i class="fa fa-circle-o"></i> Penilaian <small>Cari silabus / RPP untuk diberi penilaian</small></h3>
@endsection
@section('body')

 <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
 <form id="form-validation" action="{{url('kepala+sekolah/guru/simpan')}}" method="post" class="form-horizontal form-box remove-margin">
   @method('POST')
   @csrf
   <!-- Form Header -->
   <h4 class="form-box-header">Pilih Nama Guru <small>Tentukan nama guru </small></h4>

   <!-- Form Content -->
   <div class="form-box-content">
      <div class="form-group">
         <label class="control-label col-md-2" for="nip">Nama Guru</label>
         <div class="col-md-5">
             <div class="input-group">
               <select name="nip" id="nip" class="form-control">
                  <option value=""></option>
                  @foreach ($guru as $i)
                  <option value="{{$i->nip}}">{{$i->nama}}</option>
                  @endforeach
               </select>
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
             </div>
             @if ($errors->has("nip"))
               <span class="text-danger">{{$errors->first("nip")}}</span>
             @endif
         </div>
      </div>
      
      <div class="form-group form-actions">
         <div class="col-md-10 col-md-offset-2">
             <button type="reset" class="btn btn-danger btn-sm"><i class="fa fa-repeat"></i> RESET</button>
             <button type="button" id="btnCari" class="btn btn-success btn-sm"><i class="fa fa-search"></i> CARI</button>
         </div>
     </div>
   </div>
 </form>
 <br>
 <br>
 <div id="berkas"></div>

@endsection
@section('script')
<script>
$(function(){
   $("#tbNilai").dataTable();

   $("#btnCari").click(function(){
      var nip = $("#nip").val();
      $.ajax({
         url:"{{url('kepala+sekolah/cari+berkas')}}/"+nip,
         type:'GET',
         success:function(r){
            $("#berkas").html("");
            $("#berkas").html(r);
         },
         error:function(e){
            console.log(e);
         }
      })
   });   
});
</script>
@endsection