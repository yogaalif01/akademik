@extends('kepala sekolah.template')
@section('profilsekolah','active')
@section('header')
<!-- Navigation info -->
<ul id="nav-info" class="clearfix">
   <li><a href="{{url('/kepala+sekolah')}}"><i class="fa fa-home"></i></a></li>
   <li class="active"><a href="">Profil Sekolah</a></li>
</ul>
<!-- END Navigation info -->

<!-- Your Content -->
<h3 class="page-header page-header-top"><i class="fa fa-circle-o"></i> Profil Sekolah<small> Edit dan simpan profil sekolah anda.</small></h3>
@endsection

@section('body')
<!-- Form Validation, Validation Initialization happens at the bottom of the page -->
<form id="form-validation" action="{{url('kepala+sekolah/simpanProfilSekolah')}}" method="post" class="form-horizontal form-box remove-margin">
   @method('POST')
   @csrf
   <!-- Form Header -->
   <h4 class="form-box-header">Biodata Sekolah<small>Masukkan data dengan benar</small></h4>

   <!-- Form Content -->
   <div class="form-box-content">
      <div class="form-group">
         <label class="control-label col-md-2" for="nama_sekolah">Nama Sekolah</label>
         <div class="col-md-5">
             <div class="input-group">
               <input type="text" id="nama_sekolah" name="nama_sekolah" value="{{old('nama_sekolah') <> "" ? old('nama_sekolah') : $sek->nama_sekolah}}" class="form-control">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
             </div>
             @if ($errors->has("nama_sekolah"))
               <span class="text-danger">{{$errors->first("nama_sekolah")}}</span>
             @endif
         </div>
      </div>
      <div class="form-group">
         <label class="control-label col-md-2" for="alamat_sekolah">Alamat Sekolah</label>
         <div class="col-md-5">
             <div class="input-group">
               <input type="text" id="alamat_sekolah" name="alamat_sekolah" value="{{old('alamat_sekolah') <> "" ? old('nama_sekolah') : $sek->alamat_sekolah}}" class="form-control">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
             </div>
             @if ($errors->has("alamat_sekolah"))
               <span class="text-danger">{{$errors->first("alamat_sekolah")}}</span>
             @endif
         </div>
      </div>
      <div class="form-group form-actions">
         <div class="col-md-10 col-md-offset-2">
             <button type="reset" class="btn btn-danger btn-sm"><i class="fa fa-repeat"></i> Reset</button>
             <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan</button>
         </div>
     </div>
   </div>
 </form>
@endsection
@section('script')
<script>
$(function(){

   var stssekolah = "{{session()->get('status')}}";
    if (stssekolah== 200) {
      Swal.fire(
      'Konfirmasi',
      'Simpan Berhasil',
      'success'
        )
    }

});
</script>
@endsection