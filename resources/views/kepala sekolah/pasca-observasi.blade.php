@extends('kepala sekolah.template')
@section('nilai','active')
@section('header')
<!-- Navigation info -->
<ul id="nav-info" class="clearfix">
   <li><a href="{{url('/kepala+sekolah')}}"><i class="fa fa-home"></i></a></li>
   <li class="{{url('/kepala+sekolah/penilaian.html')}}"><a href="">Penilaian</a></li>
   <li class="active"><a href="">Form Pasca Observasi Pembelajaran</a></li>
</ul>
<!-- END Navigation info -->

<!-- Your Content -->
<h3 class="page-header page-header-top"><i class="fa fa-circle-o"></i> Form Pasca Observasi Pembelajaran <small></small></h3>
@endsection

@section('body')

<table class="" width="100%" id="dataguru">
   <tr>
      <td width="20%">NIP</td>
      <td>: <b>{{ $rpp->getguru->nip }}</b></td>
   </tr>
   <tr>
      <td>Nama Guru</td>
      <td>: <b>{{ $rpp->getguru->nama }}</b></td>
   </tr>
   <tr>
      <td>Jenis Guru</td>
      <td>: <b>{{ $rpp->getguru->jenis_guru }}</b></td>
   </tr>
</table>

@if (count($rpp->getjawaban) == 0)

<form action="{{url("kepala+sekolah/pasca+observasix/simpan")}}" method="post">
<input type="hidden" name="opt" value="create">
<input type="hidden" name="nip" value="{{$rpp->getguru->nip}}">
<input type="hidden" name="idsilabus" value="{{$rpp->Id}}">
@csrf
@method("POST")
<table class="table table-bordered table-striped table-hover">
   <thead>
      <tr>
         <th width="5%">NO</th>
         <th width="45%">Pertanyaan</th>
         <th width="45%">Jawaban</th>
      </tr>
   </thead>
   <tbody>
      @php
          $no = 1;
      @endphp
      @foreach ($soal as $s)
      <tr>
         <td> {{$no++}} </td>
         <td> {{$s->soal}} </td>
         <td>
            <textarea name="jawaban{{$s->Id}}" id="jawaban" cols="30" rows="3" class="form-control">{{old("jawaban".$s->Id)}}</textarea>
            @if ($errors->has("jawaban".$s->Id))
                <span class="text-danger"> {{$errors->first("jawaban".$s->Id)}} </span>
            @endif
         </td>
      </tr>
      @endforeach
   </tbody>
</table>
<p align="center">
   <button type="submit" class="btn btn-primary">S I M P A N</button>
</p>
</form>

@else
<form action="{{url("kepala+sekolah/pasca+observasix/simpan")}}" method="post">
   <input type="hidden" name="opt" value="update">
   <input type="hidden" name="nip" value="{{$rpp->getguru->nip}}">
   <input type="hidden" name="idsilabus" value="{{$rpp->Id}}">
   @csrf
   @method("POST")
   <table class="table table-bordered table-striped table-hover">
      <thead>
         <tr>
            <th width="5%">NO</th>
            <th width="45%">Pertanyaan</th>
            <th width="45%">Jawaban</th>
         </tr>
      </thead>
      <tbody>
         @php
             $no = 1;
         @endphp
         @foreach ($soal as $s)
         <tr>
            <td> {{$no++}} </td>
            <td> {{$s->soal}} </td>
            <td>
               @foreach ($rpp->getjawaban as $j)
               @if ($j->idsoal == $s->Id)
               <input type="hidden" name="idsoal{{$s->Id}}" value="{{$s->Id}}">
               <textarea name="jawaban{{$s->Id}}" id="jawaban" cols="30" rows="3" class="form-control">{{old("jawaban".$s->Id) <> null ? old("jawaban".$s->Id) : $j->jawaban}}</textarea>
               @endif
               @endforeach
               @if ($errors->has("jawaban".$s->Id))
                   <span class="text-danger"> {{$errors->first("jawaban".$s->Id)}} </span>
               @endif
            </td>
         </tr>
         @endforeach
      </tbody>
   </table>
   <p align="center">
      <button type="submit" class="btn btn-primary">S I M P A N</button>
   </p>
   </form>
@endif

@endsection

@section('css')
<style>
#dataguru tr{
   line-height: 25px;
}
</style>
@endsection