@extends('kepala sekolah.template')
@section('soal','active')
@section('pra','active')
@section('header')
<!-- Navigation info -->
<ul id="nav-info" class="clearfix">
   <li><a href="{{url('/kepala+sekolah')}}"><i class="fa fa-home"></i></a></li>
   <li class="active"><a href="">Pertanyaan Pra Observasi</a></li>
</ul>
<!-- END Navigation info -->

<!-- Your Content -->
<h3 class="page-header page-header-top"><i class="fa fa-circle-o"></i> Pertanyaan Pra Observasi <small>Edit dan simpan soal pra observasi.</small></h3>
@endsection

@section('body')
<div class="row">
   <div class="col-lg-12">
      <a href="{{url('kepala+sekolah/soal+pra/tambah.html')}}" class="btn btn-primary btn-sm" style="margin-bottom:10px;"> <span class="fa fa-plus"></span> Tambah data pertanyaan</a>
      <br>

      <table class="table table-striped table-bordered table-hover" id="tbsoal">
         <thead>
            <tr>
               <th width="5%">#</th>
               <th>Soal / Pertanyaan</th>
               <th width="10%">aksi</th>
            </tr>
         </thead>
         <tbody>
            @php
                $no = 1;
            @endphp
            @foreach ($soal as $s)
            <tr id="{{$s->Id}}">
               <td> {{$no++}} </td>
               <td> {{$s->soal}} </td>
               <td>
                  <div class="btn-group">
                     <a data-toggle="tooltip" data-title="Edit" href="{{url('/kepala+sekolah/soal+pra/edit-'.$s->Id)}}" class="btn btn-primary btn-xs"> <span class="fa fa-pencil"></span> </a>
                     <button data-toggle="tooltip" data-title="Hapus" class="btn btn-danger btn-xs" onclick="deletex({{$s->Id}})"> <span class="fa fa-times"></span> </button>
                  </div> 
               </td>
            </tr>
            @endforeach
         </tbody>
      </table>

   </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function(){


   $("#tbsoal").DataTable();

   var stssoal = "{{session()->get('status')}}"
    if (stssoal == 200) {
      Swal.fire(
      'Konfirmasi',
      'Simpan Berhasil',
      'success'
        )
    }
})

function deletex(id){
    
    Swal.fire({
        title:"apakah anda yakin ?",
        text:"data yang sudah dihapus tidak bisa dikembalikan lagi.",
        icon:"warning",
        showCancelButton:true,
        confirmButtonColor:"#3085d6",
        cancelButtonColor:"#d33",
        confirmButtonText:"Ya, saya yakin"
    }).then((result)=>{
        if(result.value){
            $.ajax({
                url:"{{url('/kepala+sekolah/soal+pra/delete-')}}"+id,
                type:"GET",
                success:function(r){
                    var result = JSON.parse(r);
                    // console.log(result);
                    if(result["status"] == 200){
                        $("#"+id).hide();
                        Swal.fire(
                            "Berhasil !","Query berhasil dijalankan","success"
                        );
                    }
                },
                error:function(e){
                    console.log(e.responseText);
                }
            })
        }
    });
}
</script>
@endsection