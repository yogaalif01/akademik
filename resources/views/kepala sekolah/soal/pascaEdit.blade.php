@extends('kepala sekolah.template')
@section('soal','active')
@section('pra','active')
@section('header')
<!-- Navigation info -->
<ul id="nav-info" class="clearfix">
   <li><a href="{{url('/kepala+sekolah')}}"><i class="fa fa-home"></i></a></li>
   <li><a href="{{url('/kepala+sekolah/soal+pasca.html')}}"><i class="fa fa-file-text"></i></a></li>
   <li class="active"><a href="">Pertanyaan Pasca Observasi</a></li>
</ul>
<!-- END Navigation info -->

<!-- Your Content -->
<h3 class="page-header page-header-top"><i class="fa fa-circle-o"></i> Pertanyaan Pasca Observasi <small>Edit dan simpan soal pra observasi.</small></h3>
@endsection

@section('body')
<form id="form-validation" action="{{url('kepala+sekolah/soal+pasca/simpanEdit')}}" method="post" class="form-horizontal form-box remove-margin">
   @method('POST')
   @csrf
   <!-- Form Header -->
   <h4 class="form-box-header">Form data pertanyaan pra observasi <small>Masukkan data dengan benar</small></h4>
   <div class="form-box-content">
      <div class="form-group">
         <label class="control-label col-md-2" for="nip">Pertanyaan</label>
         <div class="col-md-5">
             <div class="input-group">
                <input type="hidden" name="idsoal" value=" {{$soal->Id}} ">
               <textarea name="pertanyaan" id="pertanyaan" class="form-control" cols="70" rows="3">{{old("pertanyaan") <> null ? old("pertanyaan") : $soal->soal}}</textarea>
             </div>
             @if ($errors->has("pertanyaan"))
               <span class="text-danger">{{$errors->first("pertanyaan")}}</span>
             @endif
         </div>
      </div>
      <div class="form-group form-actions">
         <div class="col-md-10 col-md-offset-2">
             <button type="reset" class="btn btn-danger btn-sm"><i class="fa fa-repeat"></i> Reset</button>
             <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan</button>
         </div>
     </div>
   </div>
</form>
@endsection