@extends('kepala sekolah.template')
@section('nilai','active')
@section('header')
<!-- Navigation info -->
<ul id="nav-info" class="clearfix">
   <li><a href="{{url('/kepala+sekolah')}}"><i class="fa fa-home"></i></a></li>
   <li class="{{url('/kepala+sekolah/penilaian.html')}}"><a href="">Penilaian</a></li>
   <li class="active"><a href="">Form Observasi Pembelajaran</a></li>
</ul>
<!-- END Navigation info -->

<!-- Your Content -->
<h3 class="page-header page-header-top"><i class="fa fa-circle-o"></i> Form Observasi Pembelajaran <small>Instrumen Pendamping Kurikulum 2013</small></h3>
@endsection
@section('body')
<table class="" width="100%" id="dataguru">
   <tr>
      <td width="20%">NIP</td>
      <td>: <b>{{ $rpp->getguru->nip }}</b></td>
   </tr>
   <tr>
      <td>Nama Guru</td>
      <td>: <b>{{ $rpp->getguru->nama }}</b></td>
   </tr>
   <tr>
      <td>Jenis Guru</td>
      <td>: <b>{{ $rpp->getguru->jenis_guru }}</b></td>
   </tr>
   <tr>
      <td>Model Pembelajaran</td>
      <td>: <b>{{ $tbmodel->nama_model }}</b></td>
   </tr>
</table>

<div class="alert alert-info" id="petunjuk">
   Berilah tanda titik (<i class="fa fa-circle"></i>) pada angka “1” bila tidak dilakukan; Pada angka “2” bila komponen ada tetapi tidak dilakukan; Pada angka “3” bila komponen ada tetapi kurang sesuai dilakukan; Pada “4” Bila komponenen lengkap dan dilakukan dengan tepat. Berikan catatan atau saran untuk perbaikan pelaksanaaan pembelajaran sesuai penilaian bapak/ibu
</div>

@if (count($rpp->getnilai) == null)

<form action="{{url("kepala+sekolah/simpan+nilai")}}" method="POST">
   @csrf
   @method("POST")
   <input type="hidden" name="idsilabus" value="{{$rpp->Id}}">
   <input type="hidden" name="idmodel" value="{{$idmodel}}">
   <input type="hidden" name="jenis" value="observasi pembelajaran">
   <table class="table table-bordered table-striped table-hover" width="100%">
      <thead>
         <tr>
            <th width="5%">NO</th>
            <th width="40%">Komponen / Aspek</th>
            <th width="20%">Hasil Telaah Skor</th>
            <th width="">Catatan</th>
         </tr>
      </thead>
      <tbody>
         @php
             $no = 1;
         @endphp
         @foreach ($kategori as $k)
         
         <tr>
            @if (strlen($k->kode) == 2)
            <td colspan="4"><b>{{$k->nama_kategori}}<b></td>
            @else
            <td><b>{{$k->kode}}<b></td>
            <td colspan="3"><b>{{$k->nama_kategori}}<b></td>
            @endif
         </tr>
            @foreach ($k->getindikator as $i)
               <input type="hidden" name="{{"model".$i->Id}}" value="{{$k->idmodel}}">
                <tr>
                  <td>{{$no++}}</td>
                  <td style="text-align:justify">{{$i->nama_indikator}}</td>
                  <td>
                     <input type="radio" name="{{"nilai".$i->Id}}" value="1" {{ old("nilai".$i->Id) == 1 ? "checked" : "" }} id=""> 1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <input type="radio" name="{{"nilai".$i->Id}}" {{ old("nilai".$i->Id) == 2 ? "checked" : "" }} value="2" id=""> 2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <input type="radio" name="{{"nilai".$i->Id}}" {{ old("nilai".$i->Id) == 3 ? "checked" : "" }} value="3" id=""> 3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <input type="radio" name="{{"nilai".$i->Id}}" {{ old("nilai".$i->Id) == 4 ? "checked" : "" }} value="4" id=""> 4 
                     <br>
                     @if ($errors->has("nilai".$i->Id))
                        <small class="text-danger">{{$errors->first("nilai".$i->Id)}}</small>
                     @endif
                  </td>
                  <td>
                     <textarea name="{{"catatan".$i->Id}}" id="" class="form-control" cols="30" rows="3">{{old("catatan".$i->Id)}}</textarea>
                     
                  </td>
                </tr>
            @endforeach
         @endforeach
      </tbody>
   </table>
   <p align="center">
      <button type="submit" class="btn btn-primary">S I M P A N</button>
   </p>
   </form>
@else
<form action="{{url("kepala+sekolah/simpan+nilai")}}" method="POST">
   @csrf
   @method("POST")
   <input type="hidden" name="idsilabus" value="{{$rpp->Id}}">
   <input type="hidden" name="idmodel" value="{{$idmodel}}">
   <input type="hidden" name="jenis" value="observasi pembelajaran">
   <table class="table table-bordered table-striped table-hover" width="100%">
      <thead>
         <tr>
            <th width="5%">NO</th>
            <th width="40%">Komponen / Aspek</th>
            <th width="20%">Hasil Telaah Skor</th>
            <th width="">Catatan</th>
         </tr>
      </thead>
      <tbody>
         @php
             $no = 1;
             $Iabjad = 0;
             $abjad = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z")
         @endphp,
         @foreach ($kategori as $k)
         <tr>
            @if (strlen($k->kode) >= 2)
            <td colspan="4"><b>{{$k->nama_kategori}}<b></td>
            @else
            @php
                $nomor = $abjad[$Iabjad];
                $Iabjad++;
            @endphp
            <td><b>{{$nomor}}<b></td>
            <td colspan="3"><b>{{$k->nama_kategori}}<b></td>
            @endif
         </tr>
            @foreach ($k->getindikator as $i)
               <input type="hidden" name="{{"model".$i->Id}}" value="{{$k->idmodel}}">
                <tr>
                  <td>{{$no++}}</td>
                  <td style="text-align:justify">{{$i->nama_indikator}}</td>
                  <td>
                     @php
                        $nilai = 0;
                        $catatan = "";
                     @endphp
                     @foreach ($rpp->getnilai as $n)
                        @if ($n->idindikator == $i->Id && $n->idmodel == $k->idmodel)
                             @php
                                 $nilai = $n->nilai;
                                 $catatan = $n->catatan;
                                 break;
                             @endphp
                        @endif
                     @endforeach
                     <input type="radio" name="{{"nilai".$i->Id}}" value="1" {{ old("nilai".$i->Id) == 1 ? "checked" : "" }} {{ $nilai == 1 ? "checked" : "" }} id=""> 1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                     <input type="radio" name="{{"nilai".$i->Id}}" {{ old("nilai".$i->Id) == 2 ? "checked" : "" }} {{ $nilai == 2 ? "checked" : "" }} value="2" id=""> 2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                     <input type="radio" name="{{"nilai".$i->Id}}" {{ old("nilai".$i->Id) == 3 ? "checked" : "" }} {{ $nilai == 3 ? "checked" : "" }} value="3" id=""> 3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                     <input type="radio" name="{{"nilai".$i->Id}}" {{ old("nilai".$i->Id) == 4 ? "checked" : "" }} {{ $nilai == 4 ? "checked" : "" }} value="4" id=""> 4 
                     <br>
                     @if ($errors->has("nilai".$i->Id))
                        <small class="text-danger">{{$errors->first("nilai".$i->Id)}}</small>
                     @endif
                  </td>
                  <td>
                     <textarea name="{{"catatan".$i->Id}}" id="" class="form-control" cols="30" rows="3">{{ old("catatan".$i->Id) <> null ? old("catatan".$i->Id) : $catatan }}</textarea>
                  </td>
                </tr>
            @endforeach
         @endforeach
      </tbody>
   </table>
   <p align="center">
      <button type="submit" class="btn btn-primary">S I M P A N</button>
   </p>
   </form>
@endif
@endsection


@section('css')
<style>
#dataguru tr{
   line-height: 25px;
}

#petunjuk{
   margin:15px 0px 15px 0px;
}
</style>
@endsection