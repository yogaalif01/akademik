@extends('kepala sekolah.template')
@section('nilai','active')
@section('header')
<!-- Navigation info -->
<ul id="nav-info" class="clearfix">
   <li><a href="{{url('/kepala+sekolah')}}"><i class="fa fa-home"></i></a></li>
   <li class="{{url('/kepala+sekolah/penilaian.html')}}"><a href="">Penilaian</a></li>
   <li class="active"><a href="">Form Penilaian RPP</a></li>
</ul>
<!-- END Navigation info -->

<!-- Your Content -->
<h3 class="page-header page-header-top"><i class="fa fa-circle-o"></i> Form Penilaian <small>Beri Nilai Berdasarkan Indikator Yang Muncul</small></h3>
@endsection
@section('body')

<table class="" width="100%" id="dataguru">
   <tr>
      <td width="20%">NIP</td>
      <td>: <b>{{ $rpp->getguru->nip }}</b></td>
   </tr>
   <tr>
      <td>Nama Guru</td>
      <td>: <b>{{ $rpp->getguru->nama }}</b></td>
   </tr>
   <tr>
      <td>Jenis Guru</td>
      <td>: <b>{{ $rpp->getguru->jenis_guru }}</b></td>
   </tr>
</table>

<div class="alert alert-info" id="petunjuk">
   Berilah tanda titik (<i class="fa fa-circle"></i>) pada angka “1” bila tidak sesuai; pada angka “2” bila komponen ada tetapi tidak sesuai; pada angka “3” bila Komponen ada tetapi kurang sesuai; pada angka “4” bila Komponen lengkap dan sesuai. Berikan catatan atau saran untuk perbaikan pelaksanaan pembelajaran sesuai penilaian bapak/ibu.
</div>

{{-- ===== nilai belum diinput ===== --}}



@if (count($rpp->getnilai) == null)
<form action="{{url("kepala+sekolah/simpan+nilai")}}" method="POST">
@csrf
@method("POST")
<input type="hidden" name="idsilabus" value="{{$rpp->Id}}">
<input type="hidden" name="idmodel" value="0">
<input type="hidden" name="jenis" value="rpp">
<table class="table table-bordered table-striped table-hover" width="100%">
   <thead>
      <tr>
         <th width="5%">NO</th>
         <th width="40%">Komponen / Aspek</th>
         <th width="20%">Hasil Telaah Skor</th>
         <th width="">Catatan</th>
      </tr>
   </thead>
   <tbody>
      @php
          $no = 1;
      @endphp
      @foreach ($kategori as $k)
      <tr>
         @if (strlen($k->kode) == 2)
         <td colspan="4"><b>{{$k->nama_kategori}}<b></td>
         @else
         <td><b>{{$k->kode}}<b></td>
         <td colspan="3"><b>{{$k->nama_kategori}}<b></td>
         @endif
      </tr>
         @foreach ($k->getindikator as $i)
         <input type="hidden" name="{{'model'.$i->Id}}" value="{{$i->idmodel}}">
             <tr>
               <td>{{$no++}}</td>
               <td style="text-align:justify">{{$i->nama_indikator}}</td>
               <td>
                  <input type="radio" name="{{"nilai".$i->Id}}" value="1" {{ old("nilai".$i->Id) == 1 ? "checked" : "" }} id=""> 1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="radio" name="{{"nilai".$i->Id}}" {{ old("nilai".$i->Id) == 2 ? "checked" : "" }} value="2" id=""> 2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="radio" name="{{"nilai".$i->Id}}" {{ old("nilai".$i->Id) == 3 ? "checked" : "" }} value="3" id=""> 3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="radio" name="{{"nilai".$i->Id}}" {{ old("nilai".$i->Id) == 4 ? "checked" : "" }} value="4" id=""> 4 
                  <br>
                  @if ($errors->has("nilai".$i->Id))
                     <small class="text-danger">{{$errors->first("nilai".$i->Id)}}</small>
                  @endif
               </td>
               <td>
                  <textarea name="{{"catatan".$i->Id}}" id="" class="form-control" cols="30" rows="3">{{old("catatan".$i->Id)}}</textarea>
                  
               </td>
             </tr>
         @endforeach
      @endforeach
   </tbody>
</table>
<p align="center">
   <button type="submit" class="btn btn-primary">S I M P A N</button>
</p>
</form>

@else

<form action="{{url("kepala+sekolah/simpan+nilai")}}" method="POST">
   @csrf
   @method("POST")
   <input type="hidden" name="idsilabus" value="{{$rpp->Id}}">
   <input type="hidden" name="idmodel" value="0">
   <input type="hidden" name="jenis" value="rpp">
   <table class="table table-bordered table-striped table-hover" width="100%">
      <thead>
         <tr>
            <th width="5%">NO</th>
            <th width="40%">Komponen / Aspek</th>
            <th width="20%">Hasil Telaah Skor</th>
            <th width="">Catatan</th>
         </tr>
      </thead>
      <tbody>
         @php
             $no = 1;
         @endphp
         @foreach ($kategori as $k)
         <tr>
            @if (strlen($k->kode) >= 2)
            <td colspan="4"><b>{{$k->nama_kategori}}<b></td>
            @else
            <td><b>{{$k->kode}}<b></td>
            <td colspan="3"><b>{{$k->nama_kategori}}<b></td>
            @endif
         </tr>
            @foreach ($k->getindikator as $i)
            <input type="hidden" name="{{'model'.$i->Id}}" value="{{$i->idmodel}}">
                <tr>
                  <td>{{$no++}}</td>
                  <td style="text-align:justify">{{$i->nama_indikator}}</td>
                  <td>
                     @foreach ($rpp->getnilai as $n)
                         @if ($n->idindikator == $i->Id && $n->nilai_utk == "rpp")
                             @php
                                 $nilai = $n->nilai;
                                 $catatan = $n->catatan;
                             @endphp
                         @endif
                     @endforeach
                     <input type="radio" name="{{"nilai".$i->Id}}" value="1" {{ old("nilai".$i->Id) == 1 ? "checked" : "" }} {{ $nilai == 1 ? "checked" : "" }} id=""> 1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                     <input type="radio" name="{{"nilai".$i->Id}}" {{ old("nilai".$i->Id) == 2 ? "checked" : "" }} {{ $nilai == 2 ? "checked" : "" }} value="2" id=""> 2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                     <input type="radio" name="{{"nilai".$i->Id}}" {{ old("nilai".$i->Id) == 3 ? "checked" : "" }} {{ $nilai == 3 ? "checked" : "" }} value="3" id=""> 3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                     <input type="radio" name="{{"nilai".$i->Id}}" {{ old("nilai".$i->Id) == 4 ? "checked" : "" }} {{ $nilai == 4 ? "checked" : "" }} value="4" id=""> 4 
                     <br>
                     @if ($errors->has("nilai".$i->Id))
                        <small class="text-danger">{{$errors->first("nilai".$i->Id)}}</small>
                     @endif
                  </td>
                  <td>
                     <textarea name="{{"catatan".$i->Id}}" id="" class="form-control" cols="30" rows="3">{{ old("catatan".$i->Id) <> null ? old("catatan".$i->Id) : $catatan }}</textarea>
                  </td>
                </tr>
            @endforeach
         @endforeach
      </tbody>
   </table>
   <p align="center">
      <button type="submit" class="btn btn-primary">S I M P A N</button>
   </p>
   </form>

@endif

@endsection

@section('css')
<style>
#dataguru tr{
   line-height: 25px;
}

#petunjuk{
   margin:15px 0px 15px 0px;
}
</style>
@endsection