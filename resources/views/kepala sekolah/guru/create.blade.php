@extends('kepala sekolah.template')
@section('guru','active')
@section('header')
<!-- Navigation info -->
<ul id="nav-info" class="clearfix">
   <li><a href="{{url('/kepala+sekolah')}}"><i class="fa fa-home"></i></a></li>
   <li><a href="{{url('/kepala+sekolah/guru.html')}}">Guru</a></li>
   <li class="active"><a href="">Tambah data guru</a></li>
</ul>
<!-- END Navigation info -->

<!-- Your Content -->
<h3 class="page-header page-header-top"><i class="fa fa-circle-o"></i>Tambah Data Guru <small>Kelola data guru</small></h3>
@endsection

@section('body')
 <!-- Form Validation, Validation Initialization happens at the bottom of the page -->
 <form id="form-validation" action="{{url('kepala+sekolah/guru/simpan')}}" method="post" class="form-horizontal form-box remove-margin">
   @method('POST')
   @csrf
   <!-- Form Header -->
   <h4 class="form-box-header">Form data guru <small>Masukkan data dengan benar</small></h4>

   <!-- Form Content -->
   <div class="form-box-content">
      <div class="form-group">
         <label class="control-label col-md-2" for="nip">NIP</label>
         <div class="col-md-5">
             <div class="input-group">
               <input type="text" id="nip" name="nip" value="{{old('nip')}}" class="form-control">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
             </div>
             @if ($errors->has("nip"))
               <span class="text-danger">{{$errors->first("nip")}}</span>
             @endif
         </div>
      </div>
      <div class="form-group">
         <label class="control-label col-md-2" for="nm_guru">Nama Guru</label>
         <div class="col-md-5">
             <div class="input-group">
               <input type="text" id="nm_guru" name="nm_guru" value="{{old('nm_guru')}}" class="form-control">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
             </div>
             @if ($errors->has("nm_guru"))
             <span class="text-danger">{{$errors->first("nm_guru")}}</span>
           @endif
         </div>
      </div>
      <div class="form-group">
         <label class="control-label col-md-2" for="alamat_guru">Alamat</label>
         <div class="col-md-5">
             <div class="input-group">
               <input type="text" id="alamat_guru" name="alamat_guru" value="{{old('alamat_guru')}}" class="form-control">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
             </div>
             @if ($errors->has("alamat_guru"))
             <span class="text-danger">{{$errors->first("alamat_guru")}}</span>
           @endif
         </div>
      </div>
      <div class="form-group">
         <label class="control-label col-md-2" for="tmp_lahir_guru">Tempat Lahir</label>
         <div class="col-md-5">
             <div class="input-group">
               <input type="text" id="tmp_lahir_guru" name="tmp_lahir_guru" value="{{old('tmp_lahir_guru')}}" class="form-control">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
             </div>
             @if ($errors->has("tmp_lahir_guru"))
             <span class="text-danger">{{$errors->first("tmp_lahir_guru")}}</span>
           @endif
         </div>
      </div>
      <div class="form-group">
         <label class="control-label col-md-2" for="tgl_lahir_guru">Tanggal Lahir</label>
         <div class="col-md-5">
             <div class="input-group">
               <input type="text" id="tgl_lahir_guru" name="tgl_lahir_guru" value="{{old('tgl_lahir_guru')}}" class="form-control input-datepicker" autocomplete="off" data-date-format="yyyy-mm-dd">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
             </div>
             @if ($errors->has("tgl_lahir_guru"))
             <span class="text-danger">{{$errors->first("tgl_lahir_guru")}}</span>
           @endif
         </div>
      </div>
      <div class="form-group">
         <label class="control-label col-md-2" for="jk_guru">Jenis Kelamin</label>
         <div class="col-md-5">
             <div class="input-group">
                @php
                    $jk = ["L","P"];
                @endphp
                <select name="jk_guru" id="jk_guru" class="form-control">
                  <option value=""></option>
                  @for ($i = 0; $i < count($jk); $i++)
                  <option value="{{$jk[$i]}}" {{old('jk_guru') == $jk[$i] ? "selected" : "" }}>{{$jk[$i] == "L" ? "Laki-laki" : "Perempuan"}}</option>
                  @endfor
                </select>
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
             </div>
             @if ($errors->has("jk_guru"))
             <span class="text-danger">{{$errors->first("jk_guru")}}</span>
           @endif
         </div>
      </div>
      <div class="form-group">
         <label class="control-label col-md-2" for="jenis_guru">Jenis Guru</label>
         <div class="col-md-5">
             <div class="input-group">
               <input type="text" id="jenis_guru" name="jenis_guru" value="{{old('jenis_guru')}}" class="form-control">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
             </div>
             @if ($errors->has("jenis_guru"))
             <span class="text-danger">{{$errors->first("jenis_guru")}}</span>
           @endif
         </div>
      </div>
      <div class="form-group">
         <label class="control-label col-md-2" for="username">Username</label>
         <div class="col-md-5">
             <div class="input-group">
               <input type="text" id="username" name="username" value="{{old('username')}}" class="form-control">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
             </div>
             @if ($errors->has("username"))
             <span class="text-danger">{{$errors->first("username")}}</span>
           @endif
         </div>
      </div>
      <div class="form-group">
         <label class="control-label col-md-2" for="password">Password</label>
         <div class="col-md-5">
             <div class="input-group">
               <input type="password" id="password" name="password" value="{{old('password')}}" class="form-control">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
             </div>
             @if ($errors->has("password"))
             <span class="text-danger">{{$errors->first("password")}}</span>
           @endif
         </div>
      </div>
      <div class="form-group form-actions">
         <div class="col-md-10 col-md-offset-2">
             <button type="reset" class="btn btn-danger btn-sm"><i class="fa fa-repeat"></i> Reset</button>
             <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan</button>
         </div>
     </div>
   </div>
 </form>
@endsection