@extends('kepala sekolah.template')
@section('guru','active')
@section('header')
<!-- Navigation info -->
<ul id="nav-info" class="clearfix">
   <li><a href="{{url('/kepala+sekolah')}}"><i class="fa fa-home"></i></a></li>
   <li class="active"><a href="">Guru</a></li>
</ul>
<!-- END Navigation info -->

<!-- Your Content -->
<h3 class="page-header page-header-top"><i class="fa fa-circle-o"></i> Data Guru <small>Kelola data guru</small></h3>
@endsection

@section('body')
<div class="row">
   <div class="col-lg-12">
      <a href="{{url('kepala+sekolah/guru/tambah.html')}}" class="btn btn-primary btn-sm" style="margin-bottom:10px;"> <span class="fa fa-plus"></span> Tambah data guru</a>

      <table class="table table-striped table-bordered table-hover" id="tbGuru" width="100%">
         <thead>
            <tr>
               <th width="5%">#</th>
               <th width="10%">NIP</th>
               <th width="30%">Nama Guru</th>
               <th width="20%">Jenis</th>
               <th width="10%">opsi</th>
            </tr>
         </thead>
         <tbody>
            @php
                $no = 1;
            @endphp
            @foreach ($guru as $itm)
            <tr id="{{$itm->Id}}">
               <td>{{$no++}}</td>
               <td>{{$itm->nip}}</td>
               <td>{{$itm->nama}}</td>
               <td>{{$itm->jenis_guru}}</td>
               <td>
                  <div class="btn-group">
                     <a data-toggle="tooltip" data-title="Edit" href="{{url('/kepala+sekolah/guru/edit-'.$itm->Id)}}" class="btn btn-primary btn-xs"> <span class="fa fa-pencil"></span> </a>
                     <button data-toggle="tooltip" data-title="Hapus" class="btn btn-danger btn-xs" onclick="deletex({{$itm->Id}})"> <span class="fa fa-times"></span> </button>
                  </div>
               </td>
            </tr>
            @endforeach
         </tbody>
      </table>

   </div>
</div>
@endsection

@section('script')

<script type="text/javascript">
  $(document).ready(function(){

     $("#tbGuru").dataTable();

    var stsguru = "{{session()->get('status')}}";
    if (stsguru == 200) {
      Swal.fire(
      'Konfirmasi',
      'Simpan Berhasil',
      'success'
        )
    }

  });

  function deletex(id){
    
    Swal.fire({
        title:"apakah anda yakin ?",
        text:"data yang sudah dihapus tidak bisa dikembalikan lagi.",
        icon:"warning",
        showCancelButton:true,
        confirmButtonColor:"#3085d6",
        cancelButtonColor:"#d33",
        confirmButtonText:"Ya, saya yakin"
    }).then((result)=>{
        if(result.value){
            $.ajax({
                url:"{{url('/kepala+sekolah/guru/delete-')}}"+id,
                type:"GET",
                success:function(r){
                    var result = JSON.parse(r);
                    // console.log(result);
                    if(result["status"] == 200){
                        $("#"+id).hide();
                        Swal.fire(
                            "Berhasil !","Query berhasil dijalankan","success"
                        );
                    }
                },
                error:function(e){
                    console.log(e.responseText);
                }
            })
        }
    });
}
</script>

@endsection