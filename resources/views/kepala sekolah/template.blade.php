<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Akademik</title>

        <meta name="description" content="uAdmin is a Professional, Responsive and Flat Admin Template created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{asset('assets/img/favicon.ico')}}">
        <link rel="apple-touch-icon" href="{{asset('assets/img/icon57.png')}}" sizes="57x57">
        <link rel="apple-touch-icon" href="{{asset('assets/img/icon72.png')}}" sizes="72x72">
        <link rel="apple-touch-icon" href="{{asset('assets/img/icon76.png')}}" sizes="76x76">
        <link rel="apple-touch-icon" href="{{asset('assets/img/icon114.png')}}" sizes="114x114">
        <link rel="apple-touch-icon" href="{{asset('assets/img/icon120.png')}}" sizes="120x120">
        <link rel="apple-touch-icon" href="{{asset('assets/img/icon144.png')}}" sizes="144x144">
        <link rel="apple-touch-icon" href="{{asset('assets/img/icon152.png')}}" sizes="152x152">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">

        <!-- Related styles of various javascript plugins -->
        <link rel="stylesheet" href="{{asset('assets/css/plugins.css')}}">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">

        <!-- Load a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="{{asset('assets/css/themes.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('swal/sweetalert2.css')}}">
        @yield('css')
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it, eg IE8) -->
        <script src="{{asset('assets/js/vendor/modernizr-respond.min.js')}}"></script>
    </head>

    <!-- Add the class .fixed to <body> for a fixed layout on large resolutions (min: 1200px) -->
    <!-- <body class="fixed"> -->
    <body>
        <!-- Page Container -->
        <div id="page-container">
            <!-- Header -->
            <!-- Add the class .navbar-fixed-top or .navbar-fixed-bottom for a fixed header on top or bottom respectively -->
            <!-- <header class="navbar navbar-inverse navbar-fixed-top"> -->
            <!-- <header class="navbar navbar-inverse navbar-fixed-bottom"> -->
            <header class="navbar navbar-inverse">
                <!-- Mobile Navigation, Shows up  on smaller screens -->
                <ul class="navbar-nav-custom pull-right hidden-md hidden-lg">
                    <li class="divider-vertical"></li>
                    <li>
                        <!-- It is set to open and close the main navigation on smaller screens. The class .navbar-main-collapse was added to aside#page-sidebar -->
                        <a href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-main-collapse">
                            <i class="fa fa-bars"></i>
                        </a>
                    </li>
                </ul>
                <!-- END Mobile Navigation -->

                <!-- Logo -->
                <a href="{{url('/guru')}}" class="navbar-brand"><img src="{{asset('assets/img/template/logo.png')}}" alt="logo"></a>

                <!-- Loading Indicator, Used for demostrating how loading of widgets could happen, check main.js - uiDemo() -->
                <div id="loading" class="pull-left"><i class="fa fa-certificate fa-spin"></i></div>

                <!-- Header Widgets -->
                <!-- You can create the widgets you want by replicating the following. Each one exists in a <li> element -->
                <ul id="widgets" class="navbar-nav-custom pull-right">
                    <!-- Just a divider -->
                    <li class="divider-vertical"></li>

                    <!-- User Menu -->
                    <li class="dropdown pull-right dropdown-user">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><img src="{{asset('assets/img/template/avatar.png')}}" alt="avatar"> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <!-- Just a button demostrating how loading of widgets could happen, check main.js- - uiDemo() -->
                            <li>
                                <a href="javascript:void(0)" class="loading-on"><i class="fa fa-refresh"></i> Refresh</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{asset('/logout.html')}}"><i class="fa fa-lock"></i> Log out</a>
                            </li>
                        </ul>
                    </li>
                    <!-- END User Menu -->
                </ul>
                <!-- END Header Widgets -->
            </header>
            <!-- END Header -->

            <!-- Inner Container -->
            <div id="inner-container">
                <!-- Sidebar -->
                <aside id="page-sidebar" class="collapse navbar-collapse navbar-main-collapse">

                    <!-- Primary Navigation -->
                    <nav id="primary-nav">
                        <ul>
                           <li>
                                <a href="{{url('/kepala+sekolah')}}" class="@yield('dashboard')"><i class=" fa fa-fire"></i>Dashboard</a>
                           </li>
                           <li>
                                <a href="{{url('/kepala+sekolah/guru.html')}}" class="@yield('guru')"><i class=" fa fa-fire"></i>Data Guru</a>
                            </li>
                            <li class="@yield('soal')">
                                <a href="#"><i class=" fa fa-fire"></i>Pertanyaan</a>
                                <ul>
                                    <li>
                                        <a href="{{url("/kepala+sekolah/soal+pra.html")}}" class="@yield('pra')"><i class="fa fa-file-text"></i>Pertanyaan Pra Observasi</a>
                                    </li>
                                    <li>
                                        <a href="{{url("/kepala+sekolah/soal+pasca.html")}}" class="@yield('pasca')"><i class="fa fa-file-text"></i>Pertanyaan Pasca Observasi</a>
                                    </li>
                                </ul>
                             </li>
                           <li>
                              <a href="{{url('/kepala+sekolah/penilaian.html')}}" class="@yield('nilai')"><i class=" fa fa-fire"></i>Penilaian</a>
                           </li>
                           <li>
                              <a href="{{url('/kepala+sekolah/profil.html')}}" class="@yield('profil')"><i class=" fa fa-fire"></i>Profil Pribadi</a>
                           </li>
                           <li>
                                <a href="{{url('/kepala+sekolah/profil+sekolah.html')}}" class="@yield('profilsekolah')"><i class=" fa fa-fire"></i>Profil Sekolah</a>
                            </li>
                           
                        </ul>
                            

                    <!-- Demo Theme Options -->
                    <div id="theme-options" class="text-left visible-md visible-lg">
                        <a href="javascript:void(0)" class="btn btn-theme-options"><i class="fa fa-cog"></i> Theme Options</a>
                        <div id="theme-options-content">
                            <h5>Color Themes</h5>
                            <ul class="theme-colors clearfix">
                                <li class="active">
                                    <a href="javascript:void(0)" class="themed-background-default themed-border-default" data-theme="default" data-toggle="tooltip" title="Default"></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="themed-background-deepblue themed-border-deepblue" data-theme="css/themes/deepblue.css" data-toggle="tooltip" title="DeepBlue"></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="themed-background-deepwood themed-border-deepwood" data-theme="css/themes/deepwood.css" data-toggle="tooltip" title="DeepWood"></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="themed-background-deeppurple themed-border-deeppurple" data-theme="css/themes/deeppurple.css" data-toggle="tooltip" title="DeepPurple"></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="themed-background-deepgreen themed-border-deepgreen" data-theme="css/themes/deepgreen.css" data-toggle="tooltip" title="DeepGreen"></a>
                                </li>
                            </ul>
                            <h5>Header</h5>
                            <label id="topt-fixed-header-top" class="switch switch-success" data-toggle="tooltip" title="Top fixed header"><input type="checkbox"><span></span></label>
                            <label id="topt-fixed-header-bottom" class="switch switch-success" data-toggle="tooltip" title="Bottom fixed header"><input type="checkbox"><span></span></label>
                            <label id="topt-fixed-layout" class="switch switch-success" data-toggle="tooltip" title="Fixed layout (for large resolutions)"><input type="checkbox"><span></span></label>
                        </div>
                    </div>
                    <!-- END Demo Theme Options -->
                </aside>
                <!-- END Sidebar -->

                <!-- Page Content -->
                <div id="page-content">
                    @yield('header')

                    @yield('body')
                    <!-- End Your Content -->
                </div>
                <!-- END Page Content -->

                <!-- Footer -->
                <footer>
                    2020 | yogaalifferdianto @jpstudio
                </footer>
                <!-- END Footer -->
            </div>
            <!-- END Inner Container -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, check main.js - scrollToTop() -->
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>

     

        <!-- Excanvas for canvas support on IE8 -->
        <!--[if lte IE 8]><script src="js/helpers/excanvas.min.js"></script><![endif]-->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="{{asset('assets/js/vendor/jquery-1.11.1.min.js')}}"%3E%3C/script%3E'));</script>

        {{-- <script src=" {{asset('assets/js/jquery-1.11.1.min.js')}} "></script> --}}

        <!-- Bootstrap.js -->
        <script src="{{asset('assets/js/vendor/bootstrap.min.js')}}"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="{{asset('assets/js/plugins.js')}}"></script>
        <script src="{{asset('assets/js/main.js')}}"></script>
        <script type="text/javascript" src="{{asset('swal/sweetalert2.js')}}"></script>
        @yield('script')
    </body>
</html>