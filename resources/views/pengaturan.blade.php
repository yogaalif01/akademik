<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Akademik</title>

        <meta name="description" content="uAdmin is a Professional, Responsive and Flat Admin Template created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{asset('assets/img/favicon.ico')}}">
        <link rel="apple-touch-icon" href="{{asset('assets/img/icon57.png')}}" sizes="57x57">
        <link rel="apple-touch-icon" href="{{asset('assets/img/icon72.png')}}" sizes="72x72">
        <link rel="apple-touch-icon" href="{{asset('assets/img/icon76.png')}}" sizes="76x76">
        <link rel="apple-touch-icon" href="{{asset('assets/img/icon114.png')}}" sizes="114x114">
        <link rel="apple-touch-icon" href="{{asset('assets/img/icon120.png')}}" sizes="120x120">
        <link rel="apple-touch-icon" href="{{asset('assets/img/icon144.png')}}" sizes="144x144">
        <link rel="apple-touch-icon" href="{{asset('assets/img/icon152.png')}}" sizes="152x152">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">

        <!-- Related styles of various javascript plugins -->
        <link rel="stylesheet" href="{{asset('assets/css/plugins.css')}}">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">

        <!-- Load a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="{{asset('assets/css/themes.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('swal/sweetalert2.css')}}">
        <style>
           #page-content{
              margin: 0px 0px 0px 0px !important;
           }

           footer{
            margin: 0px 0px 0px 0px !important;
            text-align: center;
           }
        </style>
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it, eg IE8) -->
        <script src="{{asset('assets/js/vendor/modernizr-respond.min.js')}}"></script>
    </head>

    <!-- Add the class .fixed to <body> for a fixed layout on large resolutions (min: 1200px) -->
    <!-- <body class="fixed"> -->
    <body>
        <!-- Page Container -->
        <div id="page-container">
            <!-- Header -->
            <!-- Add the class .navbar-fixed-top or .navbar-fixed-bottom for a fixed header on top or bottom respectively -->
            <!-- <header class="navbar navbar-inverse navbar-fixed-top"> -->
            <!-- <header class="navbar navbar-inverse navbar-fixed-bottom"> -->
            <header class="navbar navbar-inverse">
                <!-- Mobile Navigation, Shows up  on smaller screens -->
                <ul class="navbar-nav-custom pull-right hidden-md hidden-lg">
                    <li class="divider-vertical"></li>
                    <li>
                        <!-- It is set to open and close the main navigation on smaller screens. The class .navbar-main-collapse was added to aside#page-sidebar -->
                        <a href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-main-collapse">
                            <i class="fa fa-bars"></i>
                        </a>
                    </li>
                </ul>
                <!-- END Mobile Navigation -->

                <!-- Logo -->
                <a href="{{url('/')}}" class="navbar-brand"><img src="{{asset('assets/img/template/logo.png')}}" alt="logo"></a>

                <!-- Loading Indicator, Used for demostrating how loading of widgets could happen, check main.js - uiDemo() -->
                <div id="loading" class="pull-left"><i class="fa fa-certificate fa-spin"></i></div>

                <!-- Header Widgets -->
                <!-- You can create the widgets you want by replicating the following. Each one exists in a <li> element -->
                
                <!-- END Header Widgets -->
            </header>
            <!-- END Header -->

            

                <!-- Page Content -->
                <div id="page-content">
                    @yield('header')

                    <div class="row">
                       <div class="col-lg-12 alert alert-danger">
                          <h4>Data Sekolah dan Kepala Sekolah Belum Di isi. Harap Isikan Data Terlebih Dahulu.</h4>
                       </div>
                    </div>

                    <div class="row" style="margin-top:10px;">
                     <div class="col-lg-5">
                     <h4 class="page-header page-header-top"><i class="fa fa-circle-o"></i> Data Kepala Sekolah <small>Masukkan data dengan benar</small></h4>

                     <form id="form-validation" action="{{url('simpanPengaturan')}}" method="post" class="form-horizontal form-box remove-margin">
                        @method('POST')
                        @csrf
                     
                        <!-- Form Content -->
                        <div class="form-box-content">
                           <div class="form-group">
                              <label class="control-label col-md-4" for="nip">NIP</label>
                              <div class="col-md-6">
                                  <div class="input-group">
                                    <input type="text" id="nip" name="nip" value="{{old('nip')}}" class="form-control">
                                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                  </div>
                                  @if ($errors->has("nip"))
                                    <span class="text-danger">{{$errors->first("nip")}}</span>
                                  @endif
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-md-4" for="nm_guru">Nama Guru</label>
                              <div class="col-md-6">
                                  <div class="input-group">
                                    <input type="text" id="nm_guru" name="nm_guru" value="{{old('nm_guru')}}" class="form-control">
                                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                  </div>
                                  @if ($errors->has("nm_guru"))
                                  <span class="text-danger">{{$errors->first("nm_guru")}}</span>
                                @endif
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-md-4" for="alamat_guru">Alamat</label>
                              <div class="col-md-6">
                                  <div class="input-group">
                                    <input type="text" id="alamat_guru" name="alamat_guru" value="{{old('alamat_guru')}}" class="form-control">
                                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                  </div>
                                  @if ($errors->has("alamat_guru"))
                                  <span class="text-danger">{{$errors->first("alamat_guru")}}</span>
                                @endif
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-md-4" for="tmp_lahir_guru">Tempat Lahir</label>
                              <div class="col-md-6">
                                  <div class="input-group">
                                    <input type="text" id="tmp_lahir_guru" name="tmp_lahir_guru" value="{{old('tmp_lahir_guru')}}" class="form-control">
                                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                  </div>
                                  @if ($errors->has("tmp_lahir_guru"))
                                  <span class="text-danger">{{$errors->first("tmp_lahir_guru")}}</span>
                                @endif
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-md-4" for="tgl_lahir_guru">Tanggal Lahir</label>
                              <div class="col-md-6">
                                  <div class="input-group">
                                    <input type="text" id="tgl_lahir_guru" name="tgl_lahir_guru" value="{{old('tgl_lahir_guru')}}" class="form-control input-datepicker" autocomplete="off" data-date-format="yyyy-mm-dd">
                                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                  </div>
                                  @if ($errors->has("tgl_lahir_guru"))
                                  <span class="text-danger">{{$errors->first("tgl_lahir_guru")}}</span>
                                @endif
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-md-4" for="jk_guru">Jenis Kelamin</label>
                              <div class="col-md-6">
                                  <div class="input-group">
                                     @php
                                         $jk = ["L","P"];
                                     @endphp
                                     <select name="jk_guru" id="jk_guru" class="form-control">
                                       <option value=""></option>
                                       @for ($i = 0; $i < count($jk); $i++)
                                       <option value="{{$jk[$i]}}" {{old('jk_guru') == $jk[$i] ? "selected" : "" }}>{{$jk[$i] == "L" ? "Laki-laki" : "Perempuan"}}</option>
                                       @endfor
                                     </select>
                                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                  </div>
                                  @if ($errors->has("jk_guru"))
                                  <span class="text-danger">{{$errors->first("jk_guru")}}</span>
                                @endif
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-md-4" for="jenis_guru">Jenis Guru</label>
                              <div class="col-md-6">
                                  <div class="input-group">
                                    <input type="text" id="jenis_guru" name="jenis_guru" value="{{old('jenis_guru')}}" class="form-control">
                                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                  </div>
                                  @if ($errors->has("jenis_guru"))
                                  <span class="text-danger">{{$errors->first("jenis_guru")}}</span>
                                @endif
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-md-4" for="username">Username</label>
                              <div class="col-md-6">
                                  <div class="input-group">
                                    <input type="text" id="username" name="username" value="{{old('username')}}" class="form-control">
                                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                  </div>
                                  @if ($errors->has("username"))
                                  <span class="text-danger">{{$errors->first("username")}}</span>
                                @endif
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="control-label col-md-4" for="password">Password</label>
                              <div class="col-md-6">
                                  <div class="input-group">
                                    <input type="password" id="password" name="password" value="{{old('password')}}" class="form-control">
                                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                  </div>
                                  @if ($errors->has("password"))
                                  <span class="text-danger">{{$errors->first("password")}}</span>
                                @endif
                              </div>
                           </div>
                           
                        </div>
                      

                     </div>
                     <div class="col-lg-1">
                     </div>
                     <div class="col-lg-5">
                     <h4 class="page-header page-header-top"><i class="fa fa-circle-o"></i> Data Sekolah <small>Masukkan data dengan benar</small></h4>
                        <div class="form-box-content">
                           <div class="form-group">
                              <label class="control-label col-md-4" for="nip">Nama Sekolah</label>
                              <div class="col-md-6">
                                 <div class="input-group">
                                    <input type="text" id="nama_sekolah" name="nama_sekolah" value="{{old('nama_sekolah')}}" class="form-control">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                 </div>
                                 @if ($errors->has("nama_sekolah"))
                                    <span class="text-danger">{{$errors->first("nama_sekolah")}}</span>
                                 @endif
                              </div>
                           </div>
                        </div>

                        <div class="form-box-content">
                           <div class="form-group">
                              <label class="control-label col-md-4" for="nip">Alamat Sekolah</label>
                              <div class="col-md-6">
                                 <div class="input-group">
                                    <input type="text" id="alamat_sekolah" name="alamat_sekolah" value="{{old('alamat_sekolah')}}" class="form-control">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                 </div>
                                 @if ($errors->has("alamat_sekolah"))
                                    <span class="text-danger">{{$errors->first("alamat_sekolah")}}</span>
                                 @endif
                              </div>
                           </div>
                        </div>

                        <div class="form-group form-actions">
                              <div class="col-md-12 ">
                                 <button type="reset" class="btn btn-danger btn-sm"><i class="fa fa-repeat"></i> Reset</button>
                                 <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan</button>
                              </div>
                        </div>
                  
                    </div>
                  </form>
                    <!-- End Your Content -->
                </div>
                <!-- END Page Content -->

                
                <!-- END Footer -->
            </div>
            <!-- END Inner Container -->
        </div>
        <!-- END Page Container -->

        <footer>2020 | yogaalifferdianto @jpstudio</footer>


        <!-- Scroll to top link, check main.js - scrollToTop() -->
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>

     

        <!-- Excanvas for canvas support on IE8 -->
        <!--[if lte IE 8]><script src="js/helpers/excanvas.min.js"></script><![endif]-->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="{{asset('assets/js/vendor/jquery-1.11.1.min.js')}}"%3E%3C/script%3E'));</script>

        {{-- <script src=" {{asset('assets/js/jquery-1.11.1.min.js')}} "></script> --}}

        <!-- Bootstrap.js -->
        <script src="{{asset('assets/js/vendor/bootstrap.min.js')}}"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="{{asset('assets/js/plugins.js')}}"></script>
        <script src="{{asset('assets/js/main.js')}}"></script>
        <script type="text/javascript" src="{{asset('swal/sweetalert2.js')}}"></script>
        @yield('script')
    </body>
</html>