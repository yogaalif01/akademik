@extends('guru.template')
@section('cekhasil','active')
@section('RPP','active')
@section('header')

<ul id="nav-info" class="clearfix">
   <li><a href="{{url('/guru')}}"><i class="fa fa-home"></i></a></li>
   <li class="active"><a href="">Daftar Nilai RPP</a></li>
</ul>
<!-- END Navigation info -->

<!-- Your Content -->
<h3 class="page-header page-header-top"><i class="fa fa-circle-o"></i> Nilai RPP <small>Nilai rpp yang sudah diinput kepala sekolah.</small></h3>
@endsection

@section('body')

 <a href="{{url('/guru/observasi/'.$guru->getguru->Id)}}" target="_blank"><button class="btn btn-primary">Lihat Nilai Proses Pembelajaran</button></a>
 <br>
 <br>


<table id="example-datatables" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                            <th class="cell-small text-center hidden-xs hidden-sm">No</th>
                                <th class="text-center"><i class="fa fa-file"></i> File RPP</th>
                                <th class="hidden-xs hidden-sm hidden-md"> Nilai RPP</th>
                                <th>Observasi Pembelajaran</th>
                            </tr>
                        </thead>
                        <tbody>
                               <?php $no = 1;?>
                        	@foreach($tpnilai as $tpnilai)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$tpnilai->file}}</td>
                                <td>
                                     <div class="btn-group">
                                        <a href="{{url('/guru/detailrpp/'.$tpnilai->Id)}}" target="_blank"><button class="btn btn-primary">Lihat Nilai</button></a>
                                    </div>

                                </td>
                                <td>
                                    <div class="btn-group">
                                         <a href="{{url('/guru/observasi/'.$tpnilai->Id)}}" target="_blank"><button class="btn btn-primary">Lihat Nilai</button></a>
                                    </div>
                                </td>
                            </tr>    

                            @endforeach
                        </tbody>
                    </table>


@endsection


@section('script')

<script type="text/javascript">
    
    $(function () {
 $('#example-datatables').dataTable({columnDefs: [{orderable: false, targets: [0]}]});
    });
</script>



@endsection