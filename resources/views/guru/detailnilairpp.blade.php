@extends('guru.template')
@section('cekhasil','active')
@section('RPP','active')
@section('header')
<ul id="nav-info" class="clearfix">
   <li><a href="{{url('/kepala+sekolah')}}"><i class="fa fa-home"></i></a></li>
   <li class="{{url('/kepala+sekolah/penilaian.html')}}"><a href="">Hasil Penilaian</a></li>
   <li class="active"><a href="">Form Hasil Penilaian</a></li>
</ul>
<!-- END Navigation info -->

<!-- Your Content -->
<h3 class="page-header page-header-top"><i class="fa fa-circle-o"></i> Form Hasil Penilaian <small>Anda Bisa Melihat Hasil Penilaian yang Muncul</small></h3>
@endsection

@section('body')

<table class="" width="100%" id="dataguru">
   <tr>
      <td width="20%">NIP</td>
      <td>: <b>{{ $rpp->getguru->nip }}</b></td>
   </tr>
   <tr>
      <td>Nama Guru</td>
      <td>: <b>{{ $rpp->getguru->nama }}</b></td>
   </tr>
   <tr>
      <td>Jenis Guru</td>
      <td>: <b>{{ $rpp->getguru->jenis_guru }}</b></td>
   </tr>
</table>
<br>

<div class="alert alert-info" id="petunjuk">
  Berikut adalah Daftar hasil nilai yang diberikan oleh kepala sekolah untuk guru.
</div>

@if (count($rpp->getnilai) == null)

<div class="alert alert-danger" id="petunjuk">
  Mohon Bersabar . . . Kepala Sekolah Belum Menginputkan Nilai RPP anda. Harap Konfirmasi ke Kepala Sekolah 
</div>


@else

<form action="{{url("kepala+sekolah/simpan+nilai")}}" method="POST">
   @csrf
   @method("POST")
   <input type="hidden" name="idsilabus" value="{{$rpp->Id}}">
   <table class="table table-bordered table-striped table-hover" width="100%">
      <thead>
         <tr>
            <th width="5%">NO</th>
            <th width="40%">Komponen / Aspek</th>
            <th width="20%">Hasil Telaah Skor</th>
            <th width="">Catatan</th>
         </tr>
      </thead>
      <tbody>
         @php
             $no = 1;
         @endphp
         @foreach ($kategori as $k)
         <tr>
            @if (strlen($k->kode) == 2)
            <td colspan="4"><b>{{$k->nama_kategori}}<b></td>
            @else
            <td><b>{{$k->kode}}<b></td>
            <td colspan="3"><b>{{$k->nama_kategori}}<b></td>
            @endif
         </tr>
            @foreach ($k->getindikator as $i)
                <tr>
                  <td>{{$no++}}</td>
                  <td style="text-align:justify">{{$i->nama_indikator}}</td>
                  <td>
                     @foreach ($rpp->getnilai as $n)
                         @if ($n->idindikator == $i->Id)
                             @php
                                 $nilai = $n->nilai;
                                 $catatan = $n->catatan;
                             @endphp
                         @endif
                     @endforeach
                     <input type="radio" disabled="disabled" name="{{"nilai".$i->Id}}" value="1" {{ old("nilai".$i->Id) == 1 ? "checked" : "" }} {{ $nilai == 1 ? "checked" : "" }} id=""> 1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                     <input type="radio" disabled="disabled"  name="{{"nilai".$i->Id}}" {{ old("nilai".$i->Id) == 2 ? "checked" : "" }} {{ $nilai == 2 ? "checked" : "" }} value="2" id=""> 2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                     <input type="radio" disabled="disabled"  name="{{"nilai".$i->Id}}" {{ old("nilai".$i->Id) == 3 ? "checked" : "" }} {{ $nilai == 3 ? "checked" : "" }} value="3" id=""> 3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                     <input type="radio" disabled="disabled"  name="{{"nilai".$i->Id}}" {{ old("nilai".$i->Id) == 4 ? "checked" : "" }} {{ $nilai == 4 ? "checked" : "" }} value="4" id=""> 4 
                     <br>
                     @if ($errors->has("nilai".$i->Id))
                        <small class="text-danger">{{$errors->first("nilai".$i->Id)}}</small>
                     @endif
                  </td>
                  <td>
                     <input type="text" disabled="disabled" name="{{"catatan".$i->Id}}" id="" class="form-control" placeholder="CATATAN" value="{{ old("catatan".$i->Id) <> null ? old("catatan".$i->Id) : $catatan }}">
                  </td>
                </tr>
            @endforeach
         @endforeach
      </tbody>
   </table>
   <p align="center">
         <label class="form-control">Total Nilai Keseluruhan = {{$total}}</label> 

   </p>
   </form>

@endif

@endsection