@extends('guru.template')
@section('profil','active')
@section('header')
<!-- Navigation info -->
<ul id="nav-info" class="clearfix">
   <li><a href="{{url('/guru')}}"><i class="fa fa-home"></i></a></li>
   <li class="active"><a href="">Profil</a></li>
</ul>
<!-- END Navigation info -->

<!-- Your Content -->
<h3 class="page-header page-header-top"><i class="fa fa-circle-o"></i> Profil Pribadi <small>Ubah dan simpan data diri anda.</small></h3>
@endsection


@section('body')

 <form id="basic-wizard" action="{{url('/guru/updateProfil')}}" method="post" class="form-horizontal form-box">
  {{csrf_field()}}
    <div class="form-box-content">
    	 <div class="form-group">
             <label class="control-label col-md-2" for="example-username">No Induk Pegawai</label>
              <div class="col-md-5">
                  <div class="input-group">
                   <input type="text" id="example-username" name="nip" class="form-control" value="{{$profil->nip}}" readonly="true">
                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                 </div>
              </div>
          </div>
          <div class="form-group">
             <label class="control-label col-md-2" for="example-username">Nama Guru</label>
              <div class="col-md-5">
                  <div class="input-group">
                   <input type="text" id="example-username" name="nama" class="form-control" value="{{$profil->nama}}">
                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                 </div>
              </div>
          </div>
          <div class="form-group">
             <label class="control-label col-md-2" for="example-username">Jenis Kelamin</label>
              <div class="col-md-5">
                  <div class="input-group">

                   <select class="form-control">
                    
                     @if($profil->jeniskelamin == "L")
                      <option>PILIH SALAH SATU</option>
                     <option value="{{$profil->jeniskelamin}}" selected="selected">LAKI - LAKI</option>
                     @else
                      <option value="{{$profil->jeniskelamin}}">PEREMPUAN</option>
                     @endif
                     
                   </select>
                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                 </div>
              </div>
          </div>
          <div class="form-group">
             <label class="control-label col-md-2" for="example-username">Tempat Lahir</label>
              <div class="col-md-5">
                  <div class="input-group">
                   <input type="text" id="example-username" name="tempat_lhr" class="form-control" value="{{$profil->tempat_lhr}}">
                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                 </div>
              </div>
          </div>
         <div class="form-group">
          <label class="control-label col-md-2" for="example-username">Tanggal Lahir</label>
                                <div class="col-md-5">
                                    <div class="input-group date input-datepicker" data-date="now" data-date-format="dd-mm-yyyy">

                                        <input type="text" id="example-input-datepicker2" name="tgl_lahir" class="form-control" value="{{$profil->tgl_lahir}}">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
           <div class="form-group">
             <label class="control-label col-md-2" for="example-username">Alamat</label>
              <div class="col-md-5">
                  <div class="input-group">
                   <input type="text" id="example-username" name="alamat" class="form-control" value="{{$profil->alamat}}">
                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                 </div>
              </div>
          </div>

          <div class="form-group">
             <label class="control-label col-md-2" for="example-username">Deskripsi</label>
              <div class="col-md-5">
                  <div class="input-group">
                   <input type="text" id="example-username" name="jenis_guru" class="form-control" value="{{$profil->jenis_guru}}">
                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                 </div>
              </div>
          </div>
          <div class="form-group">
             <label class="control-label col-md-2" for="example-username">Username</label>
              <div class="col-md-5">
                  <div class="input-group">
                   <input type="text" id="example-username" name="username" class="form-control" value="{{$profil->username}}">
                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                 </div>
              </div>
          </div>
          <div class="form-group">
             <label class="control-label col-md-2" for="example-username">Password</label>
              <div class="col-md-5">
                  <div class="input-group">
                   <input type="text" id="example-username" name="password" class="form-control" value="{{$profil->password}}">
                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                 </div>
              </div>
          </div>
            <div class="form-group">
             <label class="control-label col-md-2" for="example-username">Jabatan</label>
              <div class="col-md-5">
                  <div class="input-group">
                   <input type="text" id="example-username" name="jabatan" class="form-control" value="{{$profil->jabatan}}">
                     <span class="input-group-addon"><i class="fa fa-user"></i></span>
                 </div>
              </div>
          </div>
           <div class="form-group form-actions">
                   <div class="col-md-10 col-md-offset-2">
                   <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Update</button>
                 </div>
              </div>
    </div>	

 </form>



@endsection
@section('script')

<script type="text/javascript">
  $(document).ready(function(){

    var stsguru = "{{session()->get('stsguru')}}";

    if (stsguru == 1) {
      Swal.fire(
      'Konfirmasi',
      'Simpan Berhasil',
      'success'
        )
    }

  });
</script>

@endsection