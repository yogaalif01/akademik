@extends('guru.template')
@section('nilai','active')
@section('header')

<ul id="nav-info" class="clearfix">
   <li><a href="{{url('/guru')}}"><i class="fa fa-home"></i></a></li>
   <li class="active"><a href="">Daftar Nilai Silabus</a></li>
</ul>
<!-- END Navigation info -->

<!-- Your Content -->
<h3 class="page-header page-header-top"><i class="fa fa-circle-o"></i> Nilai Silabus / RPP <small>Nilai silabus / rpp yang sudah diinput kepala sekolah.</small></h3>
@endsection

@section('body')

 <br>	
 <h3 class="page-header page-header-top">Data Nilai Guru</h3>
 <table id="example-datatables" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                            <th class="cell-small text-center hidden-xs hidden-sm">No</th>
                                <th class="hidden-xs hidden-sm hidden-md"> File Silabus/RPP</th>
                                <th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php $no = 1; ?>
                            @foreach($data as $data)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$data->file}}</td>
                                <td>{{$data->getnilai->nilai}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


@endsection

@section('script')

 <script>
 $(function () {
 $('#example-datatables').dataTable({columnDefs: [{orderable: false, targets: [0]}]});
    });
</script>

@endsection