@extends('guru.template')
@section('silabus','active')
@section('header')
<!-- Navigation info -->
<ul id="nav-info" class="clearfix">
   <li><a href="{{url('/guru')}}"><i class="fa fa-home"></i></a></li>
   <li class="active"><a href="">RPP</a></li>
</ul>
<!-- END Navigation info -->

<!-- Your Content -->
<h3 class="page-header page-header-top"><i class="fa fa-circle-o"></i> Upload RPP <small>upload silabus / rpp yang sudah anda buat disini.</small></h3>
@endsection

@section('body')
 <form action="{{url('/guru/postSilabus')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
 	{{csrf_field()}}
                        <h4 class="form-box-header">File Upload</h4>
                        <div class="form-box-content">
                            <div class="form-group">
                                <label style="" class="control-label col-md-2" for="example-file"><b>Upload File (.doc-.xls)</b></label>
                                <div class="col-md-4">
                                    <input type="file" id="example-file" name="berkas" class="form-control">
                                </div>
                                 @if ($errors->has("berkas"))
               <span class="text-danger">{{$errors->first("berkas")}}</span>
             @endif
                            </div>
                            <div class="form-group">
                                <label style="" class="control-label col-md-2" for="example-file"><b>Model Pembelajaran</b></label>
                                <div class="col-md-4">
                                <select class="form-control" name="model_pembelajaran">
                                    <option value=""> - Pilih Salah Satu - </option>
                                    <option value="Pendekatan Saintifik">Pendekatan Saintifik</option>
                                    <option value="Genre Based Approach">Genre Based Approach</option>
                                    <option value="Problem Based Learning">Problem Based Learning</option>
                                    <option value="Project Based Learning">Project Based Learning</option>
                                    <option value="Inquiry/Discovery Learning">Inquiry/Discovery Learning</option>
                                   </select>
                                </div>
                                 @if ($errors->has("model_pembelajaran"))
               <span class="text-danger">{{$errors->first("model_pembelajaran")}}</span>
             @endif
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-10 col-md-offset-2">
                                    <button class="btn btn-success" type="submit"><i class="fa fa-arrow-circle-o-up"></i> Upload</button>
                                </div>
                            </div>
                        </div>
                    </form>


                    <br>
                    <h3 class="page-header page-header-top">Data yang sudah di upload</h3>
                    <table id="example-datatables" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                            <th class="cell-small text-center hidden-xs hidden-sm">No</th>
                                <th class="text-center"><i class="fa fa-file"></i> File Upload</th>
                                <th class="hidden-xs hidden-sm hidden-md"> Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php $no = 1; ?>
                        	@foreach($silabus as $silabus)
                        	 <tr>
                        	 	<td>{{$no++}}</td>
                        	 	<td>{{$silabus->file}}</td>
                        	 	<td>
                        	 		 <div class="btn-group">
                                        <a href="" title="Delete" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#example-modal" data-id="{{$silabus->Id}}" ><i class="fa fa-times" ></i></a>
                                    </div>
                        	 	</td>
                        	 </tr>		
                        	@endforeach
                        </tbody>
                    </table>


                    <div id="example-modal" class="modal">
                                <!-- Modal Dialog -->
                                <div class="modal-dialog">
                                    <!-- Modal Content -->
                                    <div class="modal-content">
                                    	 <form id="delform" action="" method="post">
              								{{csrf_field()}}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">×</button>
                                            <h4>Konfirmasi Hapus</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Apakah File ini Ingin di hapus ??</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-danger" data-dismiss="modal">Tidak</button>
                                            <button class="btn btn-success" id="hapus">Ya</button>
                                        </div>
                                    </form>
                                    </div>
                                    <!-- END Modal Content -->
                                </div>
                                <!-- END Modal Dialog -->
                            </div>
@endsection


@section('script')
<script type="text/javascript">
  $(document).ready(function(){

    var stsguru = "{{session()->get('stsguru')}}";

    if (stsguru == 1) {
      Swal.fire(
      'Konfirmasi',
      'Simpan Berhasil',
      'success'
        )
    }

    
  });
</script>

 <script>
 $(function () {
 $('#example-datatables').dataTable({columnDefs: [{orderable: false, targets: [0]}]});
	});
</script>
<script type="text/javascript">
    $('#example-modal').on('show.bs.modal', function(e) {
     var id = $(e.relatedTarget).data('id');
     $('#example-modal').val(id);
    $('#delform').attr('action', "{{url('/guru/deletesilabus')}}" + "/"+ id);
  });
</script>

@endsection