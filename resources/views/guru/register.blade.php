<form id="" action="" method="post" class="form-horizontal">
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="input-group">
                            <input type="text" id="username" name="username" placeholder="Username.." class="form-control">
                            <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="input-group">
                            <input type="password" id="password" name="password" placeholder="Password.." class="form-control">
                            <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                  <div class="col-xs-12">
                      <div class="input-group">
                         @php
                             $jenis = array("GURU","KEPALA SEKOLAH");
                         @endphp
                          <select name="jenisuser" id="jenisuser" class="form-control" width="100%">
                           <option value="">[ DAFTAR SEBAGAI ]</option>
                           @for ($i = 0; $i < count($jenis); $i++)
                           <option value="{{$jenis[$i]}}"><span class="text-uppercase">{{$jenis[$i]}}</span></option>
                           @endfor
                          </select>
                          <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                      </div>
                  </div>
              </div>

                <div class="clearfix">
                    <div class="btn-group btn-group-sm pull-left">
                        <a href=""><button type="submit" class="btn btn-primary"><i class="fa fa-arrow-right"></i> simpan</button></a>
                    </div>
                  
                </div>
            </form>